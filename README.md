## Useful links 


[Stolen from here](https://github.com/wetw0rk/AWAE-PREP/blob/master/README.md) with some of my own additions

| Order | Name | Type | Link |
|--- | --- | --- | --- |
| 1 | JavaScript For Pentesters | Course | https://www.pentesteracademy.com/course?id=11 |
| 2 | Edabit (Javascript, Java, PHP) | Challenges | https://edabit.com/ |
| 3 | Simple Object Oriented Language Examples | Notes | N/A (I just wrote simple templates)
| 4 | From SQL Injection to Shell | Tutorial | https://pentesterlab.com/exercises/from_sqli_to_shell/ |
| 5 | XSS and MySQL | Challenge | https://www.vulnhub.com/entry/pentester-lab-xss-and-mysql-file,66/ |
| 6 | Understanding PHP Object Injection | Tutorial | https://securitycafe.ro/2015/01/05/understanding-php-object-injection/ |
| 7 | /dev/random: Pipe | Challenge | https://www.vulnhub.com/entry/devrandom-pipe,124/ |
| 8 | Understanding Java Deserialization | Tutorial | https://nytrosecurity.com/2018/05/30/understanding-java-deserialization/
| 9 | Practicing Java Deserialization Exploits | Challenge/Tutorial | https://diablohorn.com/2017/09/09/understanding-practicing-java-deserialization-exploits/ | 
| 10 | SQL Injection Attacks and Defense | Book | https://www.amazon.com/Injection-Attacks-Defense-Justin-Clarke/dp/1597499633 |
| 11 | Sololearn | Course | https://www.sololearn.com/ |
| 12 | Python request extension for Burp | Burp Ext. | https://portswigger.net/bappstore/b324647b6efa4b6a8f346389730df160 |
