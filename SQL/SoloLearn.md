# SOLOLEARN - SQL NOTES

- A database is a collection of data that is organized in a manner that facilitates ease of access

- A database is made up of tabels that store relevant information 

- A table stores and displays data ina a structured format consisting of *columns* and *rows* similar to an excel spreadsheet

- A *primary key* is a field in the table that uniquely identifies the table records

- A primary key's main features:
	- Must contain a unique value
	- Cannot contain *NULL* values

- SQL = Structured Query Language

- SQL is used to access and manipulate a database
	- MySQL is a program that understands SQL

## SQL STATEMENTS

- The SQL `SHOW` statement displays information contained in the database and its tables

```sql
SHOW DATABASES
```
```sql
SHOW TABLES FROM <DB NAME>
```

`SHOW COLUMNS` displays the information about the columns in a given table
```sql
SHOW COLUMNS FROM <table name>
```

`SHOW COLUMNS` displays the following values for each table column:  

| NAME | DESCRIPTION |
| --- | --- |
| `Field` | column name |
| `Type` | column data type |
| `Key` | indicates whether the column is indexed |
| `Default` | default value assigned to the column |
| `Extra` | may contain any additional information that is available about a given column |

```sql
USE <DB NAME>; SHOW COLUMNS FROM <TABLE NAME>
```

- The `SELECT` statement is used to select data from a database
	- The result is stored in a result table, which is called the *result-set*

- A query nay retrieve information from selected columns or from all columns in the table 

- To create a simple `SELECT` statement, specify the name(s) of the column(s) you need from the table

Syntax of the SQL `SELECT` statement:
```sql
SELECT column_list
FROM table_name
```

- `column_list` inlcudes one or more columns from which data is retrieved
- `table-name` is the name of the table from which the information is retrieved

## SQL SYNTAX RULES

- SQL allows you to run multiple queries or commands at the same time

Example:
```sql
SELECT FirstName FROM customers;
SELECT City FROM customers;
```

*Remember to end each SQL statement with a semicolon to indicate that the statement is complete and ready to be interpreted*

- SQL is case *insensitive*

All of these will produce the same results:
```sql
select City from customers;
SELECT City FROM customers;
sElEct City From customers;
```

- A single SQL statement can be placed on one or more text lines
- Muliple SQL statments can be combined onto a single line

Selecting multiple columns:
```sql
SELECT FirstName, LastName, City 
FROM customers;
```

- To retrieve all of the information contained in your tablem use `*`

Example
```sql
SELECT * FROM customers; 
```

- in situations where you have multiple duplicate records in a table, you might want to return only unique records

- The SQL `DISTINCT` keywords is used in conjunction with `SELECT` to eliminate duplicate records in a result

Syntax:
```sql
SELECT DISTINCT column_name1, column_name2
FROM table_name;
```

- You can use the `LIMIT` keyword to return a specified subset of records

Syntax
```sql
SELECT column list
FROM table_name
LIMIT [number of records];
```
```sql
SELECT ID, FirstName, LastName, City
FROM customers LIMIT 5;
```

- You can also pick up a set of records from a particular *offset*

Picking four records starting from the third position:
```sql
SELECT ID, FirstName, LastName, City
FROM customers LIMIT 3, 4;
```

- This would retuern 4 records with ID 4,5,6,7

## SORTING RESULTS 

- In SQL, you can provide the table name prior to the column name by separating them with a dot

	- This is called *fully qualified name*

These two statements are equivalent:
```sql
SELECT City FROM customers;

SELECT customers.City FROM customers;
``` 

*This form of writing is especially useful when working with multiple tables that may share the same column names*

- `ORDERBY` is used with `SELECT` to sort the returned data

This example sorts our customer table by the *FirstName* column:
```sql
SELECT * FROM customers
ORDER BY FirstName;
```

- `ORDER BY` can be used to retrieve by multiple columns

- When using `ORDER BY` with more than one column, separate the list of columns to follow `ORDER BY` with commas

To order by LastName and Age:
```sql
SELECT * FROM customers 
ORDER BY LastName, Age;
```

# FILTERING, FUNCTIONS, SUBQUERIES

## THE `WHERE` STATEMENT

- The `WHERE` clause is used to extract only those records that fulfill a specified criterion

Syntax:
```sql
SELECT column_list 
FROM table_name
WHERE condition;
```
```sql
SELECT * FROM customers
WHERE ID = 7;
```

SQL Operators:

| OPERATOR | DESCRIPTION |
| --- | --- |
| `=` | Equal |
| `!=` | Not Equal |
| `>` | Greater Than |
| `<` | Less Than |
| `>=` | Greater Than or Equal to|
| `<=` | Less Than or Equal to |
| `BETWEEN` | Between an inclusive range | 


Select all except for id 5:
```sql
SELECT * FROM customers
WHERE ID != 5;
```

- The `BETWEEN` operator selects values within a range
	- The first value must be lower bound and the second value, the upper bound

Syntax:
```sql
SELECT column_name(s)
FROM table_name
WHERE column_name BETWEEN value1 AND value2;
```

Select all records with IDs that fall between 3 and 7:
```sql
SELECT * FROM customers 
WHERE ID BETWEEN 3 AND 7;
```

Select the names of students whose ids are between 13 and 45:
```sql
SELECT id, name FROM students WHERE id BETWEEN 13 AND 45;
```

- When working with text columns, surround any text that appears in the statement with single quotation marks (`'`)

```sql
SELECT ID, FirstName, LastName, City 
FROM customers
WHERE City = 'New York';
```

## FILTERING WITH `AND`, `OR`

- Logical operators can be used to combine two Boolean values and return a result of `true`, `false`, or `null`

| OPERATOR | DESCRIPTION |
| --- | --- |
| `AND` | TRUE if both expressions are TRUE |
| `OR` | TRUE if either expression is TRUE |
| `IN` | TRUE if the operand is equal to one of a list of expressions |
| `NOT` | Returns TRUE if the expression is not TRUE |


```sql
SELECT ID, FirstName, LastName, Age
FROM customers
WHERE Age >= 30 AND Age <= 40;
```

Combining `AND` and `OR`:
```sql
SELECT * FROM customers
WHERE City = 'New York'
AND (Age=30 OR Age=35);
```

