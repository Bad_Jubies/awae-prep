# PYTHON DECORATORS

- Decorators allow you to add functionality to an existing function

```py
def hello(name='Jose'):
	print("The hello() function has been executed")

	def greet():
		return '\t This is the greet() function inside of hello!'

	def welcome():
		return '\t This is the welcome() function inside of hello!'

	print(greet())
	print(welcome())
```

```py
def hello(name='Jose'):
	print("The hello() function has been executed")

	def greet():
		return '\t This is the greet() function inside of hello!'

	def welcome():
		return '\t This is the welcome() function inside of hello!'

	if name == 'Jose':
		return greet
	else:
		return welcome

my_new_func = hello('Jose')
print(my_new_func())

>>The hello() function has been executed
>>       This is the greet() function inside of hello! 

```



```py
def new_decorator(original_func):
	def wrap_func():

		print ('Some extra code, before the original function')

		original_func()

		print('Some extra code, after the original function')

	return wrap_func

def func_needs_decorator():
	print("This needs to be decorated...")

decorated_function = new_decorator(func_needs_decorator)

decorated_function()

>> Some extra code, before the original function
>> This needs to be decorated...
>> Some extra code, after the original function
```

```py
def new_decorator(original_func):
	def wrap_func():

		print ('Some extra code, before the original function')

		original_func()

		print('Some extra code, after the original function')

	return wrap_func

@new_decorator
def func_needs_decorator():
	print("This needs to be decorated ...")

func_needs_decorator()

>> Some extra code, before the original function
>> This needs to be decorated...
>> Some extra code, after the original function

```