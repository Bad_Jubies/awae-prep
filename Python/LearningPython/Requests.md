# LEARNING THE REQUESTS LIBRARY

```py
import requests

r = requests.get('https://requests.readthedocs.io/en/master/')

# This will list the available methods of the request object
print(dir(r))

# This will give more detailed instructions on how to use the methods
print(help(r))
```

```py
import requests
r = requests.get('https://requests.readthedocs.io/en/master/')

print(r.content)
```

Passing parameters to the website:
```py
import requests
payload = {'page': 2, 'count': 25} 

r = requests.get('https://httpbin.org/get', params=payload)

print(r.text)
```

Respose:
```
{
  "args": {
    "count": "25",
    "page": "2"
  },
  "headers": {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "httpbin.org",
    "User-Agent": "python-requests/2.24.0",
    "X-Amzn-Trace-Id": "Root=1-5f4bca38-8ae1242a55647a2adb436f62"
  },
  "origin": "68.114.222.185",
  "url": "https://httpbin.org/get?page=2&count=25"
}
```
```py
print(r.url)

>> https://httpbin.org/get?page=2&count=25
```

Making a POST request to a URL:
```py
import requests

# You need to look at the source code for the web page to see which parameters are required
payload = {'username': 'MyUsername', 'password': 'mypass123'} 

r = requests.post('https://httpbin.org/post', data=payload)

print(r.text)
```

Response:
```
  "args": {},
  "data": "",
  "files": {},
  "form": {
    "password": "mypass123",
    "username": "MyUsername"
  },
  "headers": {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Content-Length": "38",
    "Content-Type": "application/x-www-form-urlencoded",
    "Host": "httpbin.org",
    "User-Agent": "python-requests/2.24.0",
    "X-Amzn-Trace-Id": "Root=1-5f4bcb0f-36eee2f4e14106122a5a9ffa"
  },
  "json": null,
  "origin": "68.114.222.185",
  "url": "https://httpbin.org/post"
}
```

```py
r_dict = r.json()

print(r_dict['form'])

>> {'password': 'mypass123', 'username': 'MyUsername'}
```


Basic Auth:
```py
import requests

r = requests.get('https://httpbin.org/basic-auth/username/password123', auth=('username', 'password123'))

print(r.text)
```

Response:
```
{
  "authenticated": true,
  "user": "username"
}
```

