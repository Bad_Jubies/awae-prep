# Objects and Data Structures

1. Integers - int - whole numbers: 3 300 200
2. Floating Points - float - numbers with a decimal point: 2.3 4.6 100.0
3. Strings - str - Ordered sequence of characters - "hello" "Sammy" "2000" 
4. Lists - list - ordered sequence of objects: [10,"hello",200.3]
5. Dictionaries - dict - Unordered "Key:Value" pairs - {"mykey":"myvalue","name":"Frank"}
6. Tuples - tup - Ordered immutable sequence of objects: (10,"hello",200.3)
7. Sets - set - Unordered collection of unique objects: {"a","b"}
8. Booleans - bool - Logical value indicating True or False

# Numbers

 - % is the "mod" operator
 	- It will tell you how many units are left after a division (aka remainder)
 ```py
7%4
3

9%2
1
 ```

# Variable assignments

```py
a = 30.1
type(a)
float
```

Indexing
```py
a = "Hello World"

print(a[0])
H

print(a[2])
l

print(a[-1])
d

print('Hello World'[8])
r
```

```py
a = "Hello"
print(a[:2])
He
print(a[2:])
llo
```
```py
a = "abcdefghijk"
print(a[4:7])
efg
```
```py
a = "abcdefghijk"
print(a[::2])
acegik
```
```py
a = "abcdefghijk"
print(a[2:7:2])
ceg
```

Reversing a string:
```py
a = "My String"
print(a[::-1])
gnirtS yM
```

Immutability - Cannot mutate or cannot change
	- Strings are immutable and items cannot be reassigned

```py
a = 'Hello World'
print(a.lower())
hello world

a = 'Hello World'
print(a.upper())
HELLO WORLD
```
## STRING INTERPOLATION


```py
print('this is a string {}'.format('INSERTED'))

this is a string INSERTED
```

```py
print('The {} {} {}'.format('fox', 'brown', 'quick'))

The fox brown quick
```

```py
print('The {2} {1} {0}'.format('fox', 'brown', 'quick'))

The quick brown fox
```
```py
result = 4/2
print("The result was {}".format(int(result)))

The result was 2
```

Float formating follows "{value:width.precision f}"

```py
result = 100/777
print("The result was {}".format(result))

The result was 0.1287001287001287
```

```py
result = 100/777
print("The result was {}".format(result))

The result was 0.1287001287001287
```

```py
result = 100/777
print("The result was {r:1.3f}".format(r=result))

The result was 0.129
```

```py
name = "Jim"
print(f"Hello my name is {name}")
```

```py
num = 100/777
print(f"Hello my name is {num:1.3}")

Hello my name is 0.129
```
```py
a = "apples"
print('I like %s' %a)

I like appples


print('I like %s' %'apples')

I like apples
```
## Lists

```py
my_list = ['alpha',2,3.7]
print(len(my_list))

3
```

```py
my_list = ['alpha',2,3.7]
print(my_list[0])

alpha
```


```py
my_list = ['alpha',2,3.7]
another_list = ['five', 5, 'seven']
print(my_list[0] + " " + another_list[2])

alpha seven
```

```py
new_list = ['one','two','three', 'four', 'five']
popped_item = new_list.pop()
print(popped_item)

five
```

```py
new_list = ['one','two','three', 'four', 'five']
popped_item = new_list.pop(2)
print(popped_item)

three
```
## DICTIONARIES

```py
my_dict  = {'key1':'value1','key2':'value2'}
print(my_dict)

{'key1':'value1','key2':'value2'}
```

```py
my_dict  = {'key1':'value1','key2':'value2'}
print(my_dict['key1'])

value1
```

```py
d = {'k1':123,'k2':[0,1,2],'k3':{'insideKey':100}}
print(d['k3']['insideKey'])

100
```

```py
d = {'k1':123,'k2':[7,9,0],'k3':{'insideKey':100}}
print(d['k2'][1])

9
```

```py
d = {'k1':100,'k2':200}
d['k3'] = 300
print(d)

{'k1': 100, 'k2': 200, 'k3': 300}
```

## TUPLES

- Tuples are very similar to lists
- Tuple values cannot be reassigned aka immutable
- Tuples are used to ensure data integrity to make sure that values do not get changed within code
- Tuples only have two methods
	- `.count()` and `.index()`


## SETS

Unordered collections of unique elements

```py
myset = set()
myset.add(1)
myset.add(2)
myset.add(2)
print(myset)

{1, 2}
```

- you can only have unique values in a set
- Sets do not have a defined order 


```py
my_list = [1,1,1,1,1,2,2,2,2,2,3,3,3,3,3]
print(set(my_list))

{1, 2, 3}
``` 

## I/O WITH BASIC FILES

```py
mylist1 = [1,2,3]
mylist2 = ['a','b','c']

for item in zip(mylist1, mylist2):
	print(item)

(1, 'a')
(2, 'b')
(3, 'c')
```

```py
'x' in ['x','y','z']
True

'a' in 'a world'
True

d = {'mykey':345}

'mykey' in {'mykey':345}
True

345 in d.keys()
False
```
```py

mylist = [10,20,30,40,50]

print(min(mylist))
10

print(max(mylist))
```

```py
mystring = 'hello'
mylist = []

for letter in mystring:
    mylist.append(letter)

print(mylist)

['h', 'e', 'l', 'l', 'o']

``` 

```py
mystring = 'hello'

mylist = [letter for letter in mystring]
print(mylist)

['h', 'e', 'l', 'l', 'o']

```

```py
celcius = [0,10,20,34.5]
fahrenheit = [((9/5)*temp + 32) for temp in celcius]
print(fahrenheit)

[32.0, 50.0, 68.0, 94.1]
```

```py
celcius = [0,10,20,34.5]
fahrenheit = []

for temp in celcius:
	fahrenheit.append(((9/5)*temp + 32))

print(fahrenheit)

[32.0, 50.0, 68.0, 94.1]
```

