# METHODS AND FUNCTIONS

Getting help for methods:
```py
my_list = [1,2,3,4]

help(mylist.<name of method>)
```	


## FUNCTIONS

Example Function Syntax:
```py
def name_of_function(name):
	'''
	Docstring - Documentation of function's purpose
	'''

	print("Hello" + name)


>> name_of_function("John")
>> Hello John
```

- Parameters and variables can be passed into the parentheses to be worked with within the function

- Functions use snake casing by convention:
	- lower-case
	- separated by underscores
	- helps readability

- The `return` keyword is used to send back the result of the function instead of just printing it out
	- `return` allows us to assign the output of the function to a new variable

```py
def add_function(num1,num2):
	return num1+num2

>>result = add_function(1,2)
>>print(result)
3
```


function to see if a number is even:
```py
def even_check(number):
	result = number % 2 == 0
	return result
```


```py
def check_even_list(num_list):

	# placeholder variables
	even_numbers = []

	for number in num_list:
		if number % 2 == 0:
			even_numbers.append(number)
		else:
			#Don't do anything and exit the loop
			pass

	return even_numbers


>>a = check_even_list([1,2,3,4,5,6,7,8,9])
>>print(a)
[2,4,6,8]
```

Working with tuples within functions
```py
work_hours = [('John',100),('Jane',400),('Jim',800)]

def employee_check(work_hours):

	current_max=0
	employee_of_month = ''

	for employee,hours in work_hours:
		if hours > current_max:
			current_max = hours
			employee_of_month = employee
		else:
			pass

	return (employee_of_month,current_max)

# tuple unpacking
name,hours = employee_check(work_hours)

print(name)
print(hours)

>> Jim
>> 800
```

Shuffling a list:
```py
from random import shuffle

example = [1,2,3,4,5,6,7]

def shuffle_list(mylist):
	shuffle(mylist)
	return mylist

>>result = shuffle_list(example)
>>print(result)

[1, 3, 4, 5, 6, 7, 2]
```

guessing game:
```py
from random import shuffle

# Initial list
mylist = ['','O','']

def shuffle_list(mylist):
	shuffle(mylist)
	return mylist

def player_guess():
	guess=''

	while guess not in ['0','1','2']:
		guess = input("Pick a number: 0, 1, or 2: ")

	# We have to cast an integer because input() will always return a string
	return int(guess)

def check_guess(mylist,guess):
	if mylist[guess] == 'O':
		print("correct")
		print(mylist)
	else:
		print("Wrong guess")
		print(mylist)


#Shuffle the list
mixed_up_list = shuffle_list(mylist)

#User Guess
guess = player_guess()

# Check Guess
check_guess(mixed_up_list,guess)

```

## `*args` and `**kwargs`

- Arguments and key-word arguments

``` py
def myfunc(*args):
	return sum(args) * 0.05

	# for item in args:
	# 	print(item) 

print(myfunc(1,2,3,34,54,6,646,6))
>> 37.300000000000004
```

```py
def myfunc(**kwargs):
	if 'keyword' in kwargs:
		print ('The keyword is {}'.format(kwargs['
			keyword']))
	else:
		print('I did not receive a keyword')

myfunc(keyword = 'Test')

>> The keyword is Test
```

```py
def myfunc(**kwargs):
	print(kwargs)

myfunc(fruit='apple',veggie='celery')

>> {'fruit': 'apple', 'veggie': 'celery'}
```

Cobining args and kwargs:
```py
# We define args first, and then kwargs
def myfunc(*args,**kwargs):
	print('I would like {} {}'.format(args[0],kwargs['food']))

# we must define args and then kwargs becuase of the order declared in the function
myfunc(25,50,12,fruit='apple',food='pancakes')

>>I would like 25 pancakes
```

```py
from random import shuffle
def Convert(string):
     list1=[]
     list1[:0]=string
     shuffle(list1)
     return list1

 print(Convert("Hello"))

>> ['l', 'H', 'e', 'l', 'o']
```

## Nested Statements and Scope

- LEGB RULE:
	
	- L: Local - Names assigned in any way within a function (def or lambda), not declared global in that function

	- E: Enclosing function locals - Names in the local scope of any and all enclosing functions (def or labmbda), from inner to outer
	
	- G: Global (module) - Names assigned at the top-level of a module file, or declared global in a def within the file 

	- B: Built-in (Python) - Names preassigned in the built-in names module (open, range, SyntaxError ...)

## Lambda Expressions, Map, and Filter Functions 

- Map - pass the function that you want to apply to each object within a list

```py
def square(num):
	return num**2

my_nums = [1,2,3,4,5]

for item in map(square,my_nums):
	print(item)

>>1
>>4
>>9
>>16
>>25

print(list(map(square,my_nums)))
[1, 4, 9, 16, 25]
```

```py
def splicer(mystring):
	if len(mystring)%2 == 0:
		return 'EVEN'
	else:
		return mystring[0]

name = ['Andy','James','John']

print(list(map(splicer,names)))

>>['EVEN', 'J', 'EVEN']
```

- Filter - will filter based on a function's condition
	- The function must return a Boolean

```py
def check_even(num):
	return num%2 == 0

my_nums = [1,2,3,4,5,6,7,8]

print(filter(check_even,my_nums))

>> <filter object at 0x00000169E4522B20>

print(list(filter(check_even,my_nums)))

>> [2, 4, 6, 8]

for n in filter(check_even, my_nums):
	print(n)

>> 2
>> 4
>> 6
>> 8
```

- Lambda expressions are expressions that are used once and then never called again
	- They do not get stored


```py
lambda num: num ** 2
```

```py
my_nums = [1,2,3,4,5,6,7,8]

list(map(lambda num: num ** 2, my_nums))

>> [1, 4, 9, 16, 25, 36, 49, 64]
```

```py
my_nums = [1,2,3,4,5,6,7,8]

list(filter(lambda num:num%2 == 0, my_nums))

>> [2, 4, 6, 8]
```