# OBJECT ORIENTED PROGRAMMING

## Attributes and Class Keywords

- Classes = user defined objects
	- Uses camel casing by convention (CapitalizeEveryWord) 



```py
class Sample():
	pass

my_sample = Sample()
print(type(my_sample))

>>	<class '__main__.Sample'>
```

```py
class ClassName(object):
	"""docstring for ClassName"""
	def __init__(self, arg):
		super(ClassName, self).__init__()
		self.arg = arg
```

- The `__init__` method is called upon whenever you create an instance of the class  

- We always start with the `self` keyword
	- This connects the method to the instance of the class
	- Allows `init` to refer to itself

```py
class Dog():
	"""docstring for Dog"""
	def __init__(self, breed):
		
		self.breed = breed

my_dog = Dog(breed='Lab')

print(my_dog.breed)

>> Lab
```

```py
class Dog():
	"""docstring for ClassName"""
	def __init__(self, mybreed):
		
		self.my_attribute = mybreed

my_dog = Dog(mybreed='Huskie')

print(my_dog.my_attribute)

>> Huskie
```

```py
class Dog():
	"""docstring for ClassName"""
	def __init__(self,breed,spots,name):
		
		self.breed = breed
		self.name = name
		self.spots = spots

my_dog = Dog(breed='Huskie',name='Spike',spots=True)

print(my_dog.spots)

>> True
```


## Inheritance and Polymorphism

```py
class Animal():

	def __init__(self):
		print("ANIMAL CREATED")

	def who_am_i(self):
		print("I am an animal")

	def eat(self):
		print("I am eating")
```
```py
class Dog(Animal):
	def __init__(self):
		Animal.__init__(self):
		print("Dog Created")
		
my_dog = Dog()

my_dog
my_dog.eat()

>>ANIMAL CREATED
>>Dog Created
>>I am eating
```

## Special Methods (Magic/Dunder Methods)

```py
my_list = [1,2,3,4,5]
print(len(my_list))
>> 5
```

```py
class Book(object):
	
	def __init__(self, title, author, pages):
		self.title = title
		self.author = author
		self.pages = pages
	
	def __str__(self):
		return f"{self.title} by {self.author}"

	def __len__(self):
		return self.pages

	def __del__(self):
		print("A book object has been deleted")
```

- `__str__` defines the string representation of a class when it is called later in the program

- `__len__` defines the length attribute

- `__del__` defines what will be done when a variable is deleted from memory

```py
class Book(object):
	
	def __init__(self, title, author, pages):
		self.title = title
		self.author = author
		self.pages = pages
	
	def __str__(self):
		return f"{self.title} by {self.author}"

	def __len__(self):
		return self.pages

	def __del__(self):
		print("A book object has been deleted")

b = Book('Test Book', 'Jimmy', 200)

print(b)
print(len(b))
del b

>> Test Book by Jimmy
>> 200
>> A book object has been deleted
```


