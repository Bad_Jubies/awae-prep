# ADVANCED PYTHON MODULES

## Collections Module

Counter Class:
```py
from collections import Counter

mylist = [1,1,1,1,1,2,2,2,2,2,3,3,3,3,3]

Counter(mylist)

>> Counter({1: 5, 2: 5, 3: 5})
```
- Counter returns a dictionary
	- Keys are always the object
	- Value is always the count 

Counter can also count the characters in a string:
```py
print(Counter('aaabbbnnnsssiiiffkljahsdfsdf'))

>> Counter({'s': 5, 'a': 4, 'f': 4, 'b': 3, 'n': 3, 'i': 3, 'd': 2, 'k': 1, 'l': 1, 'j': 1, 'h': 1})
```

```py
sentence = "How many times does each word show up in this sentece with a word?"

print(Counter(sentence.lower().split()))

>> Counter({'how': 1, 'many': 1, 'times': 1, 'does': 1, 'each': 1, 'word': 1, 'show': 1, 'up': 1, 'in': 1, 'this': 1, 'sentece': 1, 'with': 1, 'a': 1, 'word?': 1})
```


There are additional methods available with the Counter() module:
```py
letters = 'aaaabbbbnnnnggggjjjjjasaaasda'
c = Counter(letters)
print(c.most_common(2))

>> [('a', 9), ('j', 5)]
```

https://github.com/Pierian-Data/Complete-Python-3-Bootcamp/blob/master/12-Advanced%20Python%20Modules/00-Collections-Module.ipynb
```
sum(c.values())                 # total of all counts
c.clear()                       # reset all counts
list(c)                         # list unique elements
set(c)                          # convert to a set
dict(c)                         # convert to a regular dictionary
c.items()                       # convert to a list of (elem, cnt) pairs
Counter(dict(list_of_pairs))    # convert from a list of (elem, cnt) pairs
c.most_common()[:-n-1:-1]       # n least common elements
c += Counter()                  # remove zero and negative counts
```


Collections has a default dictionary. It provides a default key value whenever a KeyError would normally occur. KeyError occur when a nonexistent key is called. 
```py
from collections import defaultdict

d = defaultdict(lambda: 0)

d['correct'] = 100
print(d['correct'])

>> 100

print(d['WRONGKEY'])
>> 0
```

Named Tuples:
```py
from collections import namedtuple

# Dog = namedtuple(typename, field_names)
Dog = namedtuple('Dog', ['age','breed','name'])

sammy = Dog(age=5,breed='Husky',name='Sam')

print(sammy.age)
print(sammy.breed)
>> 5
>> Husky
```

## PYTHON SHUTIL AND OS MODULE

```py



```

## PYTHON DEBUGGER

```py

```

## PYTHON REGULAR EXPRESSIONS

- Regular expressions allow you to search for general patterns in text data
- Regular expressions allow you to describe and parse text

Example:
	- For "user@email.com"
	- We know in this case we would be searching for a pattern:
		`"text" + "@" + "text" + ".com"`
	- We can use the `re` library 
		- Phone number
			- (555)-555-5555
			- `r"(\d\d\d)-\d\d\d-\d\d\d\d`
			- or `r"(\d{3})-\d{3}-\d{4}"`



```py
import re

text = "The agent's phone number is 415-456-5897. Call soon!"
pattern = 'phone'

print(re.search(pattern,text))

>> <re.Match object; span=(12, 17), match='phone'>

pattern = 'NOT IN TEXT'
print(re.search(pattern,text))

>> None


```


```py
import re

text = "The agent's phone number is 415-456-5897. Call soon!"
pattern = 'phone'

# This will only find the first match
match = re.search(pattern,text)
print(match.span())
>> (12, 17)

print(match.end())
print(match.start())
>> 17
>> 12

# This will find all matches
matches = re.findall('phone', text)
# This will print how many matches there are
print(len(matches))
>> 1

# Get actual match-objects back as a search result
for match in re.finditer('phone', text):
	print(match)

>> <re.Match object; span=(12, 17), match='phone'>


```

Table with regex quantifiers:

https://github.com/Pierian-Data/Complete-Python-3-Bootcamp/blob/master/12-Advanced%20Python%20Modules/05-Overview-of-Regular-Expressions.ipynb

```py
import re

text = 'My phone number is 408-555-1234'

# we need to add an 'r' at the beginning of the search string to indicate that we are using backslashes for regex and not as escape strings

phone = re.search(r'\d\d\d-\d\d\d-\d\d\d\d', text)
print(phone)
>> <re.Match object; span=(19, 31), match='408-555-1234'>

print(phone.group())
>> '408-555-1234'


```

## TIMING YOUR PYTHON CODE

```py

```

 