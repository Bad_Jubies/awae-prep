# NOTES FROM SOLOLEARN

# BASICS

- JavaScript lives within an HTML document
	- JavaScript must be inserted between the `<script>` and `</script>` tags

Here is how JavaScript can look in a webpage:
```html
<HTML>

	<head>
	</head>

	<body>
		<script>

		</script>
	</body>


<HTML>
``` 

- Script that is placed in the head section will be executed before the `<body></body>` is rendered.

Hello World:
```html
<HTML>
	<head></head>
	<body>
	<script>
	 document.write("Hello World!");
	</script>
	</body>
<HTML>
```

- Similarly to HTML, tags can be used to format JavaScript

```HTML
<HTML>
	<head></head>
	<body>
	<script>
	 document.write("<h1>Hello World!</h1>");
	</script>
	</body>
<HTML>
```

- You can place scripts anywhere on a webpage, but they are typically placed inside of the `<HEAD>` tag

- The `<script></script>` tag can take two attributes
	1. Language
	2. Type

```html
<script language="javascript" type="text/javascript">
 alert("This is an alert box")
</script>
```

- Scripts can also be paced in external files
	- External scripts are useful and practical when the same code is used in a number of different webpages

To call an external script, put the name of the script file in the `src` (source) attribute of the `<script>` tag:

```html
<script type="text/javascript"src="demo.js">

</script>
```

Comments in JavaScript:
```html
<script>
	// This is a single line comment
	alert("This is an alert box");
</script>
```
```html
<script>
/*  This 
	is a
	multi-line
	comment 
*/
	alert("This is an alert box");
</script>
```

## VARIABLES

- The `var` keyword is used to declare a variable
- The equal sign is called the 'assignment operator'	
- Variable names are case-sensitive

```js
var x = 10;
```

```html
<script type="text/javascript">
	var x = 10;
	document.write(x);
</script>>
```

Naming rules for variables:

- The first character must be a letter, an underscore (_), or a dollar sign ($). Subsequent characters may be letters, digits, underscores, or dollar signs.
- Numbers are not allowed as the first character.
- Variable names cannot include a mathematical or logical operator in the name. For instance, 2*something or this+that;
- JavaScript names must not contain spaces.

## DATA TYPES

Strings:
	- Strings are any text that appears in quotes
	- You can use single or double quotes
	- You can use a quote within a string as long as they don't match the quoted surrounding the string

```js
var text = "My name is 'John'";
```

- As strings must be writtent within quotes, quotes inside the string must be handled.
	- The backslash escape character turns special characters to strings

```js
var sayHello = 'Hello world \'This is an escaped quote\'';
```

## BOOLEANS

- `true` or `false`

```js
var isActive = true;
var isHoliday = false;
```

- The Boolean value of `0` (zero),`null`,`undefined`, and an empty string will all resolve as false

- Everything with a "real" value will resolve as true

## MATH OPERATORS

- Math operators perform arithmetic functions on numbers (literals or variables)

```js
var x = 10 + 5;
document.write(x);

// Outputs 15
```

- Addition = `+`
- Subtraction = `-`
- Multiplication = `*`
- Division = `/`
- Modulus (remainder) = `%` 


- Increment and Decrement:

	- Increment:
		- Increments the numerical value of its operand by one
		- If placed before the operand, it returns the incremented value
		- If placed after the operand, it returns the original value and then increments the operand

	- Decrement:
		- Same as above, but decrements by one


Examples:
```js
var++ // post increment
++var // pre increment

var-- // posr decrement
--var // pre decrement
```

## ASSIGNMENT OPERATORS

- Used to assign values to JavaScript variables

- `=`, `+=`, `-=`,`*=`,`/=`,`%=`

```html
<script>
   	var result = 20;
    result *= 5;
	document.write(result);
</script>
// outputs 100
```

## COMPARISON OPERATORS

- Comparison opeerators are used in logical statements to determine equality or difference between variables or values

```js
var num == 10
// num == 8 will return false
```



| Operator | Description | Example |
| --- | --- | --- |
| `==` | Equal to | `5==10` false |
| `===` | Identical (equal and of same type)| `5===10` false|
| `!=` | Not equal to | `5!=10` true |
| `!===` | Not identical | `10 !== 10` false|
| `>` | Greater than | `10 > 5` true|
| `>=` | Greater than or equal to | `10 >= 5` true|
| `<` | Less than | `10 < 5` false|
| `<=` | Less than or equal to | `10 <= 5` | 

## LOGICAL OPERATORS

- `&&` "AND" Returns true, if both operands are true
- `||` "OR" Returns true, if one of the operands is true
- `!` "NOT" Returns true, if the operand is false; and false, if the operand is true

Returns `true`:
 ```html
<HTML>
	<head></head>
	<body>
	<script>
	 var a = (4 > 2) && (10 < 15);
	 document.write(a);
	</script>
	</body>
<HTML>
 ```
 
- There is also a conditional (Ternary) operator `?`:
	- `variable = (condition)?value1:value2`

Returns "Old enough":
```html
<HTML>
	<head></head>
	<body>
	<script>
	
	 var age = 22;
	 var isAdult = (age < 18)?"Too young":"Old enough";
	 document.write(isAdult);
	
	</script>
	</body>
<HTML>
```

## STRING OPERATORS

- The most useful operator for strings is concatenation, represented by the `+` sign

```html
<HTML>
	<head></head>
	<body>
	<script>
	 
	 var mystring1 = "I am";
	 var mystring2 = " a noob.";
	 document.write(mystring1 + mystring2);
	
	</script>
	</body>
<HTML>
```

# CONDITIONALS AND LOOPS

`if` statement: 
```js
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 < myNum2) {
   alert("JavaScript is easy to learn.");
}
```

`else` statement:
```js
if (expression) {
   // executed if condition is true
}
else {
   // executed if condition is false
}
```
```js
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 > myNum2) {
   alert("This is my first condition");
}
else {
   alert("This is my second condition");
}
```

`else if` statement:
```js
var course = 1;
if (course == 1) {
   document.write("<h1>HTML Tutorial</h1>");
} else if (course == 2) {
   document.write("<h1>CSS Tutorial</h1>");
} else {
   document.write("<h1>JavaScript Tutorial</h1>");
}
```

`switch` statement:
```js
switch (expression) {
  case n1: 
     statements
     break;
  case n2: 
     statements
     break;
  default: 
     statements
}
```

The  `default` keyword specifies that there is not case match:
```js
var color ="yellow";
switch(color) {
   case "blue": 
     document.write("This is blue.");
     break;
   case "red": 
     document.write("This is red.");
     break;
   case "green": 
     document.write("This is green."); 
     break;
   case "orange": 
      document.write("This is orange."); 
      break;
   default: 
      document.write("Color not found.");
}

//Outputs "Color not found."
```

The `for` loop:

- Statement 1 is executed before the loop (code block) starts
- Statement 2 defines the condition for running the loop (code block)
- Statement 3 is executed each time after the loop (code block) has been executed

```js
for (statement 1; statement 2; statement 3) {
   code block to be executed
}
```

- Statement 1 = The variable is equal to one
- Statement 2 = The condition is that `i`
 must be less than or equal to 5
- Statement 3 = increase (`i++`) the value of `i` each time the code block in the loop has been executed

```js
for (i=1; i<=5; i++) {
   document.write(i + "<br />");
}

/*
Outputs:

1
2
3
4
5

*/
```

- Statement 1 is optional and can be omitted if your values are set before the loop starts:
```js
var i = 1;
for (; i<=5; i++) {
   document.write(i + "<br />");
}
```

- Also, you can initiate more than one value in statement 1, using commas to separate them:

```js
for (i=1, text=""; i<=5; i++) {
   text = i;
   document.write(i + "<br />");
}
```

- If statement 2 returns true, the loop will start over again, if it returns false, the loop will end
	
	- Statement 2 is also optional

	- If you omit statement 2, you must provide a break inside of the loop. Otherwise the loop will never end

- Statement 3 is used to change the initial variable. It can do anything, including negative increment (`i--`), positive increment (`i = i + 15`), or anything else
	- Statement 3 is also optional and can be omitted if you increment your variables within the loop


- The `while` loop repeats through a line of code, as long as the specified condition is true

```js
while (condition){
	code block
}
```

```js
var i=0;
while (i<=10) {
   document.write(i + "<br />");
   i++;
}
```

- if you forget to increase the variable used in the condition, the loop will never end
	- ensure that the condition in the `while` loop eventually becomes false



- The `Do while` loop is a variant of the `while` loop
	- This loop will execute the code block once, before checking if the condition is true and then it will repeat the loop as long as the condition is true
```js
do{
	code block
}
while (condition);
```
```js
var i=20;
do {  
  document.write(i + "<br />");
  i++;  
}
while (i<=25); 
```

- The `break` statement "jumps out" of a loop and continues executing the code after the loop
```js
for (i = 0; i <= 10; i++) {
   if (i == 5) {
     break; 
   }
   document.write(i + "<br />");
}
```

- The `continue` statement breaks only one iteration of the loop, and continues with the next iteration
```js
for (i = 0; i <= 10; i++) {
   if (i == 5) {
      continue; 
   }
   document.write(i + "<br />");
}

/*

outputs:

1 2 3 4 5 6 7 8 9 10 

*/

```

Evaluates to 16:
```js
var sum=0; 
for(i=4; i<8; i++) {
  if (i == 6) {
    continue; 
  }
  sum += i;
}
document.write(sum);
```

```js
var day_of_week = 5

switch (day_of_week) {
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
    document.write("Working Days");
    
	break;
  case 6:
    document.write("Saturday");
    
	break;
  default:
    document.write("Today is Sunday");
    break;
}
```


# FUNCTIONS

## USER DEFINED FUNCTIONS

- The main advantage for using a function is code reuse
- The function naming schema follows the same rules as variables 

```js
function myFunction(){
	// code to be reused
}

// calling the function
myFunction()
```

- functions can take parameters
	- The fucntion parameters are the names listed in the function's definition

```js
function myFunction(param1,param2,param3){
	// some code
}


// calling the function and defining the parameter values
myFunction("param1Value","param2Value","param3Value")
```

```html
<HTML>
        <head></head>
        <body>
        <script>
        function myFunction(param1,param2){
          alert(param1 + param2)
        }
        myFunction("hello ", "world")
        
        // alerts "hello world"
        </script>
        </body>
<HTML>
```


- `return` can be used to return a value and it can be stored in a variable

```js
function myFunction(x,y){
	return x + y;
}

var a = myFunction(6,7);
document.write(a);
```

- `alert()` takes a single parameter and the text is displayed to the screen
	- line breaks can be made with `\n`
	- the user can only continue to the page by clicking "ok"

- A `prompt()` box can be used to accept user input
	- The user will have to enter either "cancel" or "ok" to proceed to the page

- The `prompt()` method takes two parameters
	1. The label, which you want to display in the text box
	2. A default string to display in the text box (optional)

- The `confirm` box is often used to have the user verify or accept something
	- The user must click "ok" or "cancel" to proceed
	- If the user clicks "ok", the box returns `true`
	- If the user clicks "cancel" the box returns `false`

```js
var result = confirm("Do you really want to leave this page?");
if (result == true){
	alert("Thanks for visiting!");
} else {
	alert("Thanks for staying with us");
}
```    

# OBJECTS

- JavaScript variables are containers for data values
- Objects are variables too, but they can contain many values

- An object is a list of values that are written as `name:value` pairs

Example:
```js
var person = {
	
	name:"John",age:31,
	favColor:"green",height:185

};
```

The values are called "properties":

| Property | Property Value |
|---|---|
| name | John |
| age | 31 |
| favColor | green |
| height | 185 |


There are two ways to access an object property:
```js
objectName.propertyName

// or

objectName['propertyName'] 
``` 

Accessing the properties of the person:
```js
var person = {
	
	name:"John",age:31,
	favColor:"green",height:185

};

var x = person.name;
var y = person['age'];

document.write(x + " is " + y + " years old.")
```

- JavaScript contains some built-in properties

The `length` property can be used to count the number of characters in a property or string:
```js
var person = {
	name:"John",age:31,
	favColor:"green",height:185
};

var x = person.name.length;
document.write(x)
// prints 4
```

print all values in an object:
```js
var person = {
	name:"John",age:31,
	favColor:"green",height:185
};
 
for(var key in person) {
	var value = person[key];
	document.write(value)
}
```

- An object method is a property that contains a function definition

- You can access an object method with `objectName.methodName()`

- In `document.write()`, the `write()` function is actually a method of the document object

```js
document.write("some text")
```

## THE OBJECT CONSTRUCTOR


Creating an object using the object literal (or initializer) syntax:
```js
var person = {
name:"John",age:42,favColor:"green"
};
```

- The object literal only allows you to create a single object
- Sometimes you will need an "object type" that can be used to create a number fo objects of a single type

The standard way to create an "object type" is to use an "object constructor" function:
```js
function person(name,age,color){
	this.name = name;
	this.age = age;
	this.favColor = color;
}
``` 

- The `this` keyword refers to the current object
	- Note that `this` is not a variable. It is a keyword and its value cannot be changed

Once you have an object constructor, you can use the `new` keyword to create new objects of the same type:
```js
var p1 = new person("Jim",42,"blue");
var p2 = new person("John",25,"green");

document.write(p1.name);
document.write(p2.name);
```

## OBJECT INITIALIZATION

Use the object literal or initializer syntax to create single objects:
```js
var John = {name: "John", age: 25};
var James = {name: "James", age: 21};
```

- Spaces and line breaks are not important
	- An object definition can span multiple lines

- Regardless of how an object is created, the syntax for accessing the properties does not change

## METHODS

- Methods are functions that are stored as object properties

The following syntax is used to create an object method:
```js
methodName = function(){ lines of code }
```

Access an object method with the following syntax:
```js
objectName.methodName()
```

- A method is a function belonging to an object
	- It can be referenced using the `this` keyword

Example:
```js
function person(name, age) {
  this.name = name;  
  this.age = age;
  this.changeName = function (name) {
    this.name = name;
  }
}

var p = new person("David", 21);
p.changeName("John");
// Now p.name equals to "John"
// 
// The changeName method changes the object's name property to its argument
```

You can also define the function outside of the constructor function and associate it with the object:
```js
function person(name, age) {
  this.name= name;  
  this.age = age;
  this.yearOfBirth = bornYear;
}
function bornYear() {
  return 2016 - this.age;
}
```

```js
function person(name, age) {
  this.name= name;  
  this.age = age;
  this.yearOfBirth = bornYear;
}
function bornYear() {
  return 2016 - this.age;
}

var p = new person("A", 22);
document.write(p.yearOfBirth());
// Outputs 1994
```

# CORE OBJECTS 

## ARRAYS

- Arrays store mutiple values (or elements) in a single variable

```js
var courses = new Array("HTML","CSS","JS");
```

Accessing an Array:
```js
var courses = new Array("HTML","CSS","JS");
var course = courses[0]; //HTML
courses[1] = "C++"; // This changes the second element
```

Attempting to access an index outside of the array returns an "undefined" value:
```js
var courses = new Array("HTML","CSS","JS");
document.write(courses[10]);
// outputs "undefined"
```

You can also declare an array, tell it the number of elements it will store, and fill it with elements:
```js
var courses = new Array(3);
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";
```

- An array uses numbers to access its elements, an object uses named to access its members

You can also declare an empty array and add elements dynamically:
```js
var courses = new Array();
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";
courses[3] = "C++";
```

You can also declare arrays using the "array literal" syntax:
```js
var courses = ["HTML","CSS","JS"];
```

- JavaScript arrays have built-in properties and methods

An array's `length` property returns the number of its elements:
```js
var courses = ["HTML","CSS","JS"];
document.write(course.length);
// Outputs 3
``` 

The `concat()` method allows you to join arrays and create an entirely new array:
```js
var c1 = ["HTML", "CSS"];
var c2 = ["JS", "C++"];
var courses = c1.concat(c2)
document.write(courses)

// Outputs HTML,CSS,JS,C++ 
```

- JavaScript does not support arrays with named indexes (also known as associative arrays)

We can use the named array syntax to create an object:
```js
var person = []; // an empty array
person["name"] = "John";
person["age"] = 25;
document.write(person["age"]);
// Outputs 25
``` 

- "person" is treated as an object instead of being an array
	
	- The names indexes "name" and "age" become properties of the "person" object


## THE MATH OBJECT

- The math object allows you to perform mathematical tasks and includes several properties

| Properties | Description |
| --- | --- |
| E | Euler's constant | 
| LN2 | Natural log of the value 2 |
| LN10 | Natural log of the value 10 |
| LOG2E |  The base 2 log of Euler's constant (E) |
| LOG10E | The base 10 log of Euler's constant (E) |
| PI | Returns the constant of PI |

Example:
```js
document.write(Math.PI);
// Outputs 3.1415926...
```

- The math object contains many methods that can be used for calculations
- https://www.w3schools.com/js/js_math.asp

## THE DATE OBJECT

- The `setInterval()` method calls a function or evaluates an expression at apecified intervals (in milliseconds)

- The function will continue until `clearInterval()` is called or the windows is closes

Example:
```js
function myAlert(){
	alert("Hi");
}
setInterval(myAlert, 3000);
```

- The `Date` object allows you to work with dates

- A date consists of a year, month, a day, an hour, a second, and milliseconds

Using `new Date()` to create a date object with the current date and time:
```js
var d = new Date();
```

The other way to initialize dates allows for the creation of new dat objects from the specified date and time:
```js
new Date(milliseconds)
new Date(dateString)
new Date(year,month,day,hours,minutes,seconds,milliseconds)
```

```js
//Fri Jan 02 1970 00:00:00
var d1 = new Date(86400000); 

//Fri Jan 02 2015 10:42:00
var d2 = new Date("January 2, 2015 10:42:00");

//Sat Jun 11 1988 11:42:00
var d3 = new Date(88,5,11,11,42,0,0);
```

- When a date object is created, a number of methods make it possible to perform operations on it

| Method | Description |
| --- | --- |
| `getFullYear()` | gets the year |
| `getMonth()` | gets the month |
| `getDate()` | gets the day of the month |
| `getDay()` | gets the day of the week |
| `getHours()` | gets the hour |
| `getMinutes()` | gets the minutes |
| `getSeconds()` | gets the seconds |
| `getMilliseconds()` | gets the milliseconds |


```js
var d = new Date();
var hours = d.getHours();
// hours is equal to the current hour
```

This program prints the current time to the browser every second:
```js
varfunction printTime() {
  var d = new Date();
  var hours = d.getHours();
  var mins = d.getMinutes();
  var secs = d.getSeconds();
  document.body.innerHTML = hours+":"+mins+":"+secs;
}
setInterval(printTime, 1000);
```

# DOM AND EVENTS

- When you open a webpage in a browser, the HTML of the webpage is loaded an rendered visually on the screen
	
	- To accomplish this, the browser builds the "Document Object Model" of that page
		- This is an object orianted model of its logical structure

- The DOM represents a document as a tree structure
	- HTML elements become interrelated nodes in the tree
	- All of the nodes in a tree have some kind of relations among each other
	- Nodes can have child nodes
	- Nodes on the same tree level are called siblings 

- Example: https://www.w3schools.com/js/js_htmldom.asp

- The predefined `document` object can be used to access all elements in the DOM

- the `document` object is the owner (or root) of all the objects in the webpage

If you want to access objects in an HTML page, you always start with accessing the document object:
```js
document.body.innerHTML = "Some text";
```

- `innerHTML` is a property


## SELECTING ELEMENTS

- All HTML elements are objects
	- Every object has proprties and methods

- The `document` object has methods that allow you to select the desired HTML element

These are the three most common methods for selecting HTML elements:
```js
// finds elements by id
document.getElementById(id)

//finds elements by class name
document.getElementsByClassName(name) 

//finds elements by tag name
document.getElementsByTagName(name)
```

Select the element where `id="demo"` and change its content. This prints "Hello World!" to the screen:
```html
<HTML>
	<head></head>
	<body>
	<div id="demo"></div>
	<script>
	 
	var elem = document.getElementById("demo");
	elem.innerHTML = "Hello World!";
	
	</script>
	</body>
<HTML>
```

- The `getElementsByClassName()` method returns a collection of all elements in the document with the specified class name 

For example, if the HTML page contained three elements with `class="demo"`, the following code would return all of those elements as an array:
```js
var arr = document.getElementsByClassName("demo");
// Accessing the second element
arr[1].innerHTML = "H";
``` 

The `getElementByTagName()` method returns all of the elements of the specified tag name as an array:
```html
<p>hi</p>
<p>hello</p>
<p>hi</p>
<script>
var arr = document.getElementsByTagName("p");
for (var x = 0; x < arr.length; x++) {
  arr[x].innerHTML = "Hi there";
}
</script>
```

- Each element in the DOM has a set of properties and methods that provide information about their relationships in the DOM

| Name | Description |
| --- | --- |
| `element.childNodes` | returns an array of an element's child nodes |
| `element.firstChild` | rtetur	the first child node of an element |
| `element.lastChild` | returns the last child node of an element |
| `element.hasChildNodes` | returns true if an element has any child nodes, otherwise false|
| `element.nextSibling` | returns the next node on the same tree level |
| `element.previousSibling` | returns the previous node at the same tree level |
| `element.parentNode` | returns the parent node of an element |

```html
<html>
  <body>
    <div id ="demo">
      <p>some text</p>
      <p>some other text</p>
    </div>

    <script>
     var a = document.getElementById("demo");
     var arr = a.childNodes;
     for(var x=0;x<arr.length;x++) {
       arr[x].innerHTML = "new text";
     }
    </script>

  </body>
</html>
```

## CHANGING ATTRIBUTES

- Once you've selected the elements that you want to work with, you can change their attributes

Changing the `src` attribute of an image:
```html
<img id="myimg" src="orange.png" alt="" />
<script>
var el = document.getElementById("myimg");
el.src = "apple.png";
</script>
```` 

We can change the `href` attribute to a link:
```html
<a href="http://www.example.com">Some link</a>
<script>
var el = document.getElementsByTagName("a");
el[0].href = "http://www.sololearn.com";
</script>
```

Select all images of the page and change their `src` attribute:
```js
var arr = document.
    getElementsByTagName("img");
for(var x=0; x<arr.length; x++) {
  arr[x].src= "demo.jpg";
}
```

Changing the style of HTML elements:
```html
<div id="demo" style="width:200px">some text</div>
<script>
  var x = document.getElementById("demo");
  x.style.color = "6600FF";
  x.style.width = "100px";
</script>
```

## CREATING ELEMENTS

| Name | Description |
| --- | --- |
| `element.cloneNode()` | Clones an element an returns the resulting node |
| `document.createElement(element)` | creates a new element node |
| `document.TextNode(text)` | creates a new text node |
| `document.createTextNode(text)` | creates a new text node |
| element.appendChild(newNode) | adds a new child node to an element as the last child node |
| `element.insertBefore(node1, node2)` | inserts node1 as a child before node2 |

For Example:
```js
var node = document.createTextNode("Some new text");
```

- This will create a new text node, but it will not appear in the document unil it is appended to an existing element with `.appendChild()`  or `.insertBefore()`

Example:
```html
<div id ="demo">some content</div>

<script>
  //creating a new paragraph
  var p = document.createElement("p");
  var node = document.createTextNode("Some new text");
  //adding the text to the paragraph
  p.appendChild(node);

  var div = document.getElementById("demo");
  //adding the paragraph to the div
  div.appendChild(p);
</script>
```

## REMOVING ELEMENTS

- To remove an HTML element, you must select the parent of the element and use the `removeChild(node)` method

Example:
```html
<div id="demo">
  <p id="p1">This is a paragraph.</p>
  <p id="p2">This is another paragraph.</p>
</div>

<script>
var parent = document.getElementById("demo");
var child = document.getElementById("p1");
parent.removeChild(child);
</script>
```

## REPLACING ELEMENTS

- To replace an HTML element, the `replaceChild(newNode, oldNode)` method is used

```html
<div id="demo">
  <p id="p1">This is a paragraph.</p>
  <p id="p2">This is another paragraph.</p>
</div>

<script>
var p = document.createElement("p");
var node = document.createTextNode("This is new");
p.appendChild(node);

var parent = document.getElementById("demo");
var child = document.getElementById("p1");
parent.replaceChild(p, child);
</script>
```

## CREATING ANIMATIONS

An HTML page with a box element that will be animated using JavaScript:
```html
<style>
#container {
  width: 200px;
  height: 200px;
  background: green;
  position: relative;
}
#box {
  width: 50px;
  height: 50px;
  background: red;
  position: absolute;
}
</style>
<div id="container">
   <div id="box"> </div>
</div>
```

```js
var pos = 0; 
//our box element
var box = document.getElementById("box");
var t = setInterval(move, 10);

function move() {
  if(pos >= 150) {
    clearInterval(t);
  }
  else {
    pos += 1;
    box.style.left = pos+"px";
  }
}
```

## HANDLING EVENTS

- You can write JavaScript that executes when an event occurs, such as when a user clicks on an HTML element, moves the cursor, or submits a form
	- when an event occurs on the target element, a `handler` function is executed 

Some common HTML events:


| Event | Description | 
| --- | --- |
| `onclick` | occurs when the user click on an element |
| `onload` | occurs when an object has loaded |
| `onunload` | occurs once a page has unloaded (for `<body>`)|
| `onchange` | occurs when the content of a form element, the selection, or the checked state have changed (for `<input>`,`<keygen>`,`<select>`, and `<textarea>`)|
| `onmouseover` | occurs when the pointer is moved onto an element, or onto one of its children |
| `onmouseout` | occurs when a user moves the mouse pointer out of an element, or out of one of its children |
| `onmousedown` | occurs when the user presses a mouse button over an element |
| `onmouseup` | occurs when a user releases a mouse button over an element |
| `onblur` | occurs when an element loses focus |
| `onfocus` | occurs when an element gets focus |

Corresponding events can be added to HTML elements as attributes:
```html
<p onclick="someFunc()">some text</p>
```

Displaying an alert popup when a user clicks on a specified button:
```html
<button onclick="show()">Click Me</button>
<script type="text/javascript">
function show(){
	alert("Hi here");
}
</script>
```

Event handlers can be assigned to elements:
```js
var x = document.getElementById("demo");
x.onclick = function(){
	document.body.innerHTML = Date()
}
```

```html
<body onload = "doSomething()">
```

`window.onload` can be used to run code after the whole page is loaded:
```js
window.onload = function(){
	// some code
}
```

The `onchange` event is mostly used on textboxes. The event handler gets called when the test inside the textbox changes and focus is lost form the element:
```html
<input type="text" id="name" onchange="change()">
<script type="text/javascript">
function change(){
	var x = document.getElementById("name");
	x.value = x.value.toUpperCase();
}
</script>
```

- The addEventListener() method attaches an event handler to an element without overwriting existing event handlers
	
	- You can add *many* event handlers to one element
	- You can also add many event handlers of the same type to one element, i.e. two "click" events

```js
element.addEventListener(event,function,useCapture);
```
- The first is the event's type (like "click" or "mousedown") 

- The second parameter is the function we want to call when the event occurs

- The third parameter is a Boolean value specifying whether to use *event bubbling* or *event capturing* 
	- This parameter is optional

Example:
```js
element.addEventListener("click",myFunction);
element.addEventListener("mouseover",myFunction);

function myFunction(){
	alert("Hello World!");
}
```

This adds two event listeners to the element:
```js
element.removeEventListener("mouseover", myFunction);
```

Let's create an event handler that removes itself after being executed:
```html
<button id="demo">Start</button>

<script>
var btn = document.getElementById("demo");
btn.addEventListener("click", myFunction);

function myFunction(){
	alert(Math.random());
	btn.removeEventListener("click", myFunction);
}
</script>
```

- After clicking the button, an alert with a random number displays and the event listener is removed

## EVENT PROPAGATION

- There are two ways of event propagation in the HTML DOM
	
	1. `bubbling` - the innermost element's event is handled first and then the outer element's event is handled
		- The `<p>` element's click event
		- Bubbling goes up the DOM


	2. `capturing` - the outermost element's event is handled first and then the inner
		- The `<div>` element's click event is handled first, followed by the `<p>` element's click event
		- Capturing goes down the DOM



- The `addEventListener()` method allows you to specify the propagation type with the "useCapture" parameter

```js
addEventListener(event,function,useCapture)
```

- The default value is false, which means the bubbling propagation is used
-When the value is set to true, the event uses capture propagation

```js
// Capturing propagation
elem1.addEventListener("click",myFunction,true);

// Bubbling propagation
elem2.addEventListener("click",myFunction,false);
``` 

- This is particularly helpful when you have the same event handled for multiple elements in the DOM hierarchy

## CREATING AN IMAGE SLIDER

- The images will be changed using the "Next" and "Prev" buttons

```html
<div>
 <button> Prev </button>
 <img id="slider" src="http://somewebsite/someimage.png" width="200px" height="100px"/>
 <button> Next </button>
</div>
```

```js
var images = [
	"http://somewebsite/someimage.png",
	"http://somewebsite/someimage2.png",
	"http://somewebsite/someimage3.png"
];
```

Completed page:

HTML:
```html
<div>
  <button onclick="prev()"> Prev </button>
  <img id="slider" src="http://www.sololearn.com/uploads/slider/1.jpg" 
    width="200px" height="100px"/>
  <button onclick="next()"> Next </button>
</div>
```

JS:
```js
  var images = [
  "http://www.sololearn.com/uploads/slider/1.jpg", 
  "http://www.sololearn.com/uploads/slider/2.jpg", 
  "http://www.sololearn.com/uploads/slider/3.jpg"
  ];
  var num = 0;

function next() {
  var slider = document.getElementById("slider");
  num++;
  if(num >= images.length) {
    num = 0;
  }
  slider.src = images[num];
  }

function prev() {
  var slider = document.getElementById("slider");
  num--;
  if(num < 0) {
    num = images.length-1;
  }
  slider.src = images[num];
}
```

- The `num` variable holds the current image
- The next and previous button clicks are handled by their corresponding functions, which change the sourse of the image to the next/previous image in the array

## FORM VALIDATION

- HTML5 adds some attributes that allow forma validation
- For example, the `required` attribute can be added to an input field to make it mandatory to fill in
- More complex form validation can be done using JavaScript
- The form element has an `onsubmit` event that can be handled to perform validation

Creating a form with two inputs and one button. The text in both fields should be the same and not blank to pass the validation:

HTML:
```html
<form onsubmit="return validate()" method="post">
  Number: <input type="text" name="num1" id="num1" />
  <br />
  Repeat: <input type="text" name="num2" id="num2" />
  <br />
  <input type="submit" value="Submit" />
</form>
```

JS:
```js
function validate() {
  var n1 = document.getElementById("num1");
  var n2 = document.getElementById("num2");
  if(n1.value != "" && n2.value != "") {
    if(n1.value == n2.value) {
      return true;
    }
  }  
  alert("The values should be equal and not blank");
  return false;

}
```

- The form will submit to its action if `onsubmit` returns true

# ECMASCRIPT 6

- ECMAScript (ES) is a scripting language specification created to standardize JS
	- ES6 adds new syntax for writing complex applications, including classes and modules, iterators and for/of loops, generators, arroe functions, binary data, types arrays ... etc.

## ES6 VARIABLES AND Strings


In ES6, there are 3 ways to declare a variable:
```js
var a = 10;
const b = 'hello';
let c = true;
```

- The type of declaration depends on the necessary scope
	- Scope defines the visibility of the variable

- `var` defines a variable globallym or locally for an entire function regardless of the block scope

- `let` allows you to declare variables that are limited in scope to the block, statement, or expression in which they are used 

Example
```js
if (true){
	let name = 'James';
}
alert(name); // results in an error 
```

- In this case, the `name` variable is accessible only in the scope of the `if` statement because it was declared as `let`

Example:
```js
function varTest(){
	var x = true;
	if (true){
		var x = 2; // same variable
		console.log(x); // 2 
	}
	console.log(x); // 2
}

function letTest(){
	let x = 1;
	if (true){
		let x = 2; // different variable
		console.log(x); // 2 
	}
	console.log(x); // 1 
}
```

One of the best uses for `let` is in loops:
```js
for (let i = 0;i < 3;i++){
	document.write(i);
}
```

- Here, the `i` variable is accessible only within the scope of the `for` loop, where it is needed

- `const` variables have the same scope as variables declared using `let`
	- The difference is that `const` variables are immutable, meaning that they cannot be reassigned

The following generates an exception:
```js
const a = 'Hello';
a = 'Bye';
```

## TEMPLATE LITERALS IN ES6

- Template literals are a way to output variabls in the string
- Prior to ES6, you had to break the string 

Example prior to ES6:
```js
let name = 'David';
let msg = 'Welcome ' + name + '!';
console.log(msg);
```

Example in ES6:
```js
let name = 'David';
let msg = `Welcome ${name}!`;
console.log(msg);
```
- Notice that the literals are enclosed by the backtick(\`\`) character instead of double or single quotes

- The `${expression}` is a placeholder and can include any expression, which will get evaluated and inserted into the template literal

Example:
```js
let a = 8;
let b = 34;
let msg = `The sum is ${a+b}`;
console.log(msg);
```

## FUNCTIONS AND LOOPS IN ES6

Javascript:
```js
let arr = [1, 2, 3];
for (let k = 0; k < arr.length; k++) {
  console.log(arr[k]);
}
```

Using the `for ... in` loop to iterate over the enumerable keys of an object: 
```js
let obj = {a: 1, b: 2, c: 3};
for (let v in obj) {
  console.log(v);
}
```

ES6 introduces the `for ... of` loop:
```js
let list = ["x", "y", "z"];
for (let val of list) {
  console.log(val);
}
```

- During each iteration the `val` variable is assigned the corresponding element in the list

The `for ... of` loop works for other iterable objects as well, including strings:
```js
for (let ch of "Hello"){
	console.log(ch);
} 
``` 
