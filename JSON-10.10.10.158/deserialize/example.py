class Dog:
  # The init method defines the attributes to be assigned when the object is created
	def __init__(self, name, breed):
		# name and breed are attributes
		self.name = name
		self.breed = breed

	def bark(self):
		print(self.name + "says WOOF!")

# Now we can create an object that belongs to the Dog class 
d1 = Dog(name="Spike",breed="Golden Retriever")

print(d1.name)
# Outputs: Spike
print(d1.breed)
# Outputs: Golden Retriever
d1.bark()
# Outputs: Spike says WOOF!
