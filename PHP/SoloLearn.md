# SOLOLEARN PHP NOTES

- PHP (Hypertext Preprocessor) is an open source scripting language
- PHP scripts are executed on the server

# MODULE 1: BASIC SYNTAX

- PHP starts with `<?php` and ends with `?>`

```php
<?php
	// PHP code goes here
?>
```

Here is what PHP looks like in a webpage 
```html
<!DOCTYPE html>
<html>
<head>
	<title>My first PHP page</title>
</head>
<body>
<?php
	echo "Hello World!";
?>
</body>
</html>
```

Alternatively, you can include PHP in the HTML `<script>` tag:
```html
<html>
<head>
	<title>PHP PAGE</title>
</head>
<body>
<script language="php">
	echo "Hello World!";
</script>
</body>
</html>
```

- NOTE: *The latest version of PHP removed support for html tags*

Sometimes you can just use shorthand tags if they are supported by the server:
```php
<?
	echo "Hello World";
?>
```

- PHP has built in `echo` function to output text
- `echo` is not a function, it's a *language contruct* which means it does not require parentheses

- Each PHP statement must end with a semicolon

HTML markup can be added to the texzt in the `echo` statement:
```php
<?php
	echo "<strong> This is bold text. <strong>"
?>
```


- Comments are denoted with `//`
- Multi-line comments are denoted with `/*` and `*/`

# MODULE 2: VARIABLES

Declaring a variable in PHP:
```php
$variable_name = value;
```

Rules for PHP variables:

1. A variable must start with a letter or an underscore
2. A variable name cannot start with a number
3. A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _)
4. Variable names are case-sensitive (`$name` and `$NAME` are two different vairbales)

```php
<?php
	$name = 'john';
	$age = 25;
	echo $name;

	// outputs john
?>
```

- PHP automatically converts variables to the correct data type depending on its value

## CONSTANTS

- *Constants* are similar to variables except that they cannot be changed or 
undefined after they have been declared

- A constant's name starts with a letter or an underscore

To create a constant, use the `define()` function:
```php
define(name,value,case-sensitive)
```

Parameters:

1. name: specifies the name of the constant
2. value: specifies the value of the constant
3. case-sensitive: specifies whether the constant should be case-sensitive. Default is false

Creating a constant with a case-sensitive name:
```php
<?php
define("MSG","Hello world",true);
echo MSG;

// outputs Hello world
// using the variable msg would result in an error
?>
```

## DATA TYPES

- String, Integer, Float, Boolean, Array, Object, Null, and Resource

Strings are single or double quotes:
```php
<?php
	$string1 = "Hello World"; // comment
	$string2 = 'This is a string';
?>
```

- An integer is a whole number that cannot contain commas or blanks, must not have a decimal point, and can be positive or negative

```php
<?php
	$int1 = 42; //positive
	$int2 = -45; // negative
?>
```

Floats have a decimal:
```php
<?php
	$x = 43.56;
?>
```

Boolean:
```php
<?php
	$x = true;$y = false;
?>
```

- Most of the data types can be used in combination with one another

Example of a string and integer being put together to determine the sum of two numbers:
```php
<?php
	$str = "10";
	$int = 20;
	$sum = $str + $int;
	echo ($sum);

	// Outputs 30
?>
```

## VARIABLE SCOPE

- PHP variables can be declared anywhere in the script
- The scope is the part of the script in which the variable can be referenced or used

- PHP's most used variable scopes are *local* and *global*
- A variable declared outstide of a function has a *global scope*
- A variable declared within a function has a *local scope*, and can only be accessed within the function

Example:
```php
<?php
	$name = 'John';
	function getName(){
		echo $name;
	}
	getName();

	// Error undefined variable: name
?>
```

- The `$name` variable hasa global scope and is not accessible within the `getName()` function

- The `global` keyword is used to access a global variable from within a function

Example:
```php
<?php
	$name = 'David';
	function getName(){
		global $name;
		echo $name;
	}
	getName();

	// Outputs David
?>
```

## VARIABLE VARIABLES

- In PHP, you can use one variable to specify another variable's name
	- A *variable variable* treats another variable's name as its value

Example:
```php
<?php
	$a = 'hello';
	$hello = "Hi!";
	echo $$a;

	// Outputs Hi!
?>
```

# MODULE 3: OPERATORS

- `+`, `-`, `*`, `/`, `%`

Increment and Decrement:
```php
<?php
	
	$x++; // Equivalent to $x = $x+1;

	$x--; // Equivalent to $x = $x-1;

?>
```

Increment and Decrement operators can precede or follow a variable:
```php
<?php
	$x++; // post-increment 
	$x--; // post-decrement 
	++$x; // pre-increment 
	--$x; // pre-decrement
?>
```

The difference is that the post-increment returns the original variable before it changed the variable, while the pre-increment changes the variable first and then returns the value:
```php
<?php
	$a  = 2; $b = $a++; // $a=3,  $b=2
	$a  = 2; $b = ++$a; // $a=3,  $b=3
?>
```

Assignment operators are used to write values to variables:

| Assignment | Same as ... | Description |
| --- | --- | --- |
| `x+=y` | `x = x + y` | Addition |
| `x-=y` | `x = x - y` | Subtraction |
| `x*=y` | `x = x * y` | Multiplication |
| `x/=y` | `x = x / y` | Division |
| `x%=y` | `x = x % y` | Modulus |


```php
<?php
 $x = 50;
 $x += 100;
 echo $x;

 // Outputs: 150
?> 
```

Comparison Operators:

| Operator | Name | Result |
| --- | --- | --- |
| `==` | Equal | Returns true if equal values |
| `===` | Identical | Returns true if equal and of same type |
| `!=` | Not Equal | Returns true if not equal |
| `<>` | Not Equal | Returns true if not equal |
| `!==` | Not Identical | Returns true if not equal and not of same type |


Logical Operators:

| Operator | Name | Example | Result |
| --- | --- | --- | --- |
| `and` | And | `$x and $y` | True if both x and y are true |
| `or` | Or | `$x or $y` | True if x or y is true |
| `xor` | Xor | `$x xor $y` | True if either x or y is true, but not both |
| `&&` | And | `$x && $y` | True if both x and y are true |
| `||` | Or | `$x || $y` | True if x or y is true |
| `!` | Not | `!$x` | True if x is not true |


# MODULE 4: ARRAYS

- An *array* is a special variable which can hold more than one value at a time

## NUMERIC ARRAYS

```php
$names = array("David","Amy","John");

echo $names[0]; // David
echo $names[1]; // Amy
echo $names[2]; // John
``` 

You can have multiple data types in a single array:
```php
<?php
$myArray[0] = "John";
$myArray[1] = "<strong>PHP</strong>";
$myArray[2] = 21;

echo "$myArray[0] is $myArray[2] and knows $myArray[1]";

// Outputs "John is 21 and knows PHP"
?>
```

## ASSOCIATIVE ARRAYS

- Associative arrays are arrays that use name keys that you assign to them

There are two ways to create associative arrays:
```php
$people = array("David"=>"27", "Amy"=>"21", "John"=>"42");
// or
$people['David'] = "27";
$people['Amy'] = "21";
$people['John'] = "42";
```

```php
$people = array("David"=>"27", "Amy"=>"21", "John"=>"42");

echo $people['Amy']; // Outputs 21"
```

## MULTI-DIMENSIONAL ARRAYS

 - A multi-dimensional array contains one or more arrays

 - The dimension of an array indicates the number of indices you would need to select an element

 	- For a two-dimensional array, you need two indices to select an element
 	- For a three-dimensional array, you need three indices to select an element etc ...

*Arrays more than three levels deep are difficult to manage*

Creating a two-dimensional array that contains 3 arrays:
```php
$people = array(
   'online'=>array('David', 'Amy'),
   'offline'=>array('John', 'Rob', 'Jack'),
   'away'=>array('Arthur', 'Daniel')
);
```

- Now the two-dimensional `$people` array contains 3 arrays, and it has two indices: row and column

To access the elements of the `$people` array, we mnust point to the two indices:
```php
echo $people['online'][0]; //Outputs "David"

echo $people['away'][1]; //Outputs "Daniel"
```

# MODULE 5: CONTROL STRUCTURE

## CONDITIONAL STATEMENTS

If else:
```php
if (condition) {
   code to be executed if condition is true;
} else {
   code to be executed if condition is false;
}
```

Else if:
```php
if (condition) {
  code to be executed if condition is true;
} elseif (condition) {
  code to be executed if condition is true;
} else {
   code to be executed if condition is false;
}
```

While loop:
```php
while (condition is true) {
   code to be executed;
}
```

Do while loop:
```php
do {
  code to be executed;
} while (condition is true); 
```

For loop:
```php
for (init; test; increment) {
   code to be executed;
}
```

- Parameters:
	- `init`: Initialize the loop counter value
	- `test`: Evaluates each time the loop is iterated, continuing if evaluates to true and ending if evaluates to false
	- `increment`: Increases the loop counter value

*Each of the parameter expressions can be empty or contain multiple expressions that are separated with commas.
In the for statement, the parameters are separated with semicolons.*

```php
for ($a = 0; $a < 6; $a++) {
   echo "Value of a : ". $a . "<br />";
}

// Value of a : 0
// Value of a : 1
// Value of a : 2
// Value of a : 3
// Value of a : 4
// Value of a : 5
```

The foreach loop (There are two syntaxes):
```php
foreach (array as $value) {
  code to be executed;
}
//or
foreach (array as $key => $value) {
   code to be executed;
}
```

- The first syntax loops over the array
	- On each iteration, the value of the current element is assigned to `$value` and the array pointer if moved by one, until it reached the last array element

- The second syntax will additionally assign the current element's key to the `$key` variable on each iteration

Example:
```php
$names = array("John", "David", "Amy");
foreach ($names as $name) {
   echo $name.'<br />';
}

// John
// David
// Amy
```

Switch statement:
```php
switch (n) {
  case value1:
    //code to be executed if n=value1
    break;
  case value2:
     //code to be executed if n=value2
     break;
  ...
  default:
    // code to be executed if n is different from all labels
}
```

The default statement is used if no match is found:
```php
$x=5;
switch ($x) {
  case 1:
    echo "One";
    break;
  case 2:
    echo "Two";
    break;
  default:
    echo "No match";
}

//Outputs "No match"
```

- Failing to specify the break statement causes PHP to continue executing statements that follow the `case` until it finds a `break`

	- This behavior can be used if you need to arrive at the same output for more than one case

Example:
```php
$day = 'Wed';

switch ($day) {
  case 'Mon':
    echo 'First day of the week';
    break;
  case 'Tue':
  case 'Wed':
  case 'Thu':
    echo 'Working day';
    break;
  case 'Fri':
    echo 'Friday!';
    break;
  default:
    echo 'Weekend!';
}

//Outputs "Working day"
```

Code will keep executing if no `break` statement is present:
```php
$x=1;
switch ($x) {
  case 1:
    echo "One";
  case 2:
    echo "Two";
  case 3:
    echo "Three";
  default:
    echo "No match";
}

//Outputs "OneTwoThreeNo match"
```

 - When used with a looping structure, the `continue` statement allows for skipping over what remains of the current loop iteration

 	- It then continues the execution at the condition evaluation and moves on to the beginning of the next iteration

Example:
```php
for ($i=0; $i<10; $i++) {
  if ($i%2==0) {
    continue;
  }
  echo $i . ' ';
}

// Output: 1 3 5 7 9
```

- The `include` and `require` statements allow for the insertion of the content of one PHP file into another PHP file, before the server executes it 

Example - Header file:
```php
<?php
  echo '<h1>Welcome</h1>';
?>
```

Including the header in subsequent pages:\
```html
<html>
  <body>

  <?php include 'header.php'; ?>

  <p>Some text.</p>
  <p>Some text.</p>
  </body>
</html>
```

- The `require` statement is identical to `include`, except upon failure, the `require` statement will produce a fatal error


# MODULE 6: FUNCTIONS

- A function will not be executed immediately when a page loads, it will be executed when it is called

- Function names can only begin with a letter or underscore

```php
function functionName(){
	// code to be executed
}
```

Using parameters:
``` php
function multiplyByTwo($number) {
  $answer = $number * 2;
  echo $answer;
}
multiplyByTwo(3);
//Outputs 6
```

```php
function multiply($num1, $num2) {
  echo $num1 * $num2;
}
multiply(3, 6);
//Outputs 18
```

Default arguments:
```php
function setCounter($num=10) {
   echo "Counter is ".$num;
}
setCounter(42);  //Counter is 42
setCounter();  //Counter is 10
```


A function can return a value using the `return` statement:
```php
function mult($num1, $num2) {
  $res = $num1 * $num2;
  return $res;
}

echo mult(8, 3);
// Outputs 24
```

```php
function func($arg)  {
  $result = 0;
  for($i=0; $i<$arg; $i++) {
    $result = $result + $i;
  }
  return $result;
}
echo func(5);

// Outputs 10
```

# MODULE 7: PREDEFINED VARIABLES

- A *superglobal* is a predefined variable that is always accessible, regardless of scope
- You can access PHP superglobals through any function, class, or file

PHP's superglobal variables are `$_SERVER`, `$GLOBALS`, `$_REQUEST`, `$_POST`, `$_GET`, `$_FILES`, `$_ENV`, `$_COOKIE`, `$_SESSION`

- `$_SERVER` is an array that includes information such as headers, paths, and script locations
	- The entries in this array are created by the web server

`$_SERVER['SCRIPT_NAME']` returns tha path of the current script:
```php
<?php
echo $_SERVER['SCRIPT_NAME'];
//Outputs "/somefile.php"
?>
```

`$_SERVER['HTTP_HOST']` returns the Host header from the current request:
```php
<?php
echo $_SERVER['HTTP_HOST'];
//Outputs "localhost"
?>
```

- This method can be useful when you have a lot of images on your server and you need to transfer the website to another host

Instead of changing the path for each image, you can do the following:
```php
<?php

// Create a config.php file that holds the path to your images

$host = $_SERVER['HTTP_HOST'];
$image_path = $host.'/images/';

?>
```

Use the config.php file in your scripts:
```php
<?php
require 'config.php';
echo '<img src="'.$image_path.'header.png" />';
?>
```

The main elements of `$_SERVER`:

| Element/Code | Description |
| --- | --- |
| `$_SERVER['PHP_SELF']` | Returns the filename of the currently executing script |
| `$_SERVER['SERVER_ADDR']` | Returns the IP Address of the host server |
| `$_SERVER['SERVER_NAME']` | Returns the name of the host server |
| `$_SERVER['HTTP_HOST']` | Returns the Host header from the current request |
| `$_SERVER['REMOTE_ADDR']` | Returns the IP Address from where the user is viewing the current page |
| `$_SERVER['REMOTE_HOST']` | Returns the hostname from where the user is viewing the current page |
| `$_SERVER['REMOTE_PORT']` | Returns the port being used on the user's machine to communicate to the web server |
| `$_SERVER['SCRIPT_FILENAME']` | Returns the absolute pathname of the currently executing script |
| `$_SERVER['SERVER_PORT']` | Returns the port on the server machine being used by the web server for communication (such as 80) |
| `$_SERVER['SCRIPT_NAME']` | Returns the path of the current script |
| `$_SERVER['SCRIPT_URI']` | Returns the URI of the current page | 



- The purpose of the PHP superglobals `$_GET` and `$_POST` is to collect data that has been entered into a form

This example shows a simple HTML form that includes two input fields and a submit buton:
```html
<form>
<p>Name: <input type="text" name="name"></p>
<p>Age: <input type="text" name="age"></p>
<p><input type="submit" name="submit" value="Submit"></p>
</form>
```

- The `action` attribute specifies that when the form is submitted, the data is sent to a PHP file

- HTML form elements have names, which will be used when accessing the data with PHP



```html
<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	Welcome: <?php echo $_POST["name"]; ?><br />
	Your age: <?php echo $_POST['age']; ?>
</body>
</html>
```

- The `$_POST` superglobal array holds key/value pairs 

- In the pairs, keys are the names of the form controls and values are the input data entered by the user


submit a form to a page called my.php using the POST method
```html
<form action="my.php" method="post">
</form>
```

- The two methods for submitting forms are GET and POST 

- Information sent from a form via the POST methd is invisible to others, since all names and/or values are embedded within the body of the HTTP request 

- There are no limits on the information to be sent

- POST supports advanced functionality such as support for multi-part binary data input while uploading files to the server 

 - Information sent via the GET method is visible to everyone (all variabled names and values are displayed in the URL)

 - GET also sets limits on the amount of information that can be sent (about 2000 characters)

 - Because variables are displayed in the URL, it is possible to bookmark the page 

 Example:
```html
<form action-"actionGet.php" method="get">
	Name: <input type="text" name="name" /><br /><br />
	Age: <input type="text" name="age" /> <br /> <br />
	<input type="submit" name="submit" value="Submit" />
</form>
```

Contents of actionGet.php:
```php
<?php
	echo "Hi ".$_GET['name'].". ";
	echo "You are ".$_GET['age']." years old.";
?>
```

- GET should never be used for sending passwords or other sensitive data 

## SESSIONS

- Using a session you can store information in variables to be used across multiple pages

- Inofrmation is not stored on the user's computer as it is with *cookies*

- By default, session variables last until the user closes the browser

- A session is started using the `session_start()` function

Use the PHP global `$_SESSION` to set session variables:
```php
<?php
// Start the session
session_start();

$_SESSION['color'] = "red";
$_SESSION['name'] = "John";
?>
```

- Now the name and color variables are accessible on multiple pages throughout the entire session 

Creating another page to access the session variables:
```html
<?php
// Start the session 
session_start();
?>

<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<?php
	echo "Your name is " .$_SESSION['name'];
	// Outputs "Your name is John"
?>
</body>
</html>
```

- Your session variabels remain available in the `$_SESSION` superglobal until you close your session

- All global session variables can be removed manually using `session_unset()`

- You can also destroy the session with `session_destroy()`

## COOKIES

- Cookies are often used to identify the user

- A cookie is a small file that the server embeds on the user's computer
	- Each time the same computer requests a page through a browser, it will send the cookie too

- With PHP, you can create and retrieve cookie values

Create cookies using the `setcookie()` function:
```php
setcookie(name, value, expire, path, domain, secure, httponly);
```

| Parameter | Description |
| --- | --- |
| `name` | Specifies the cookie's name |
| `value` | Specifies the cookie's value |
| `expire` | Specifies (in seconds) when the cookie is to expire. The value `time()+86400*30` will set the cookie to expire in 30 days. If this parameter is omitted or set to 0, the cookie will expire at the end of the session (when the browser closes). Default is 0 |
| `path` | Specifies the server path of the cookie. If set to `/`, the cookie will be available within the entire domain. If set to `/php/`, the cookie will only be available within the php directory in which the cookie is being set. |
| `domain` | Specifies the cookie's domain name. To make the cookie available on all subdomains of "example.com", set the domain to "example.com". |
| `secure` | Specifies whether or not the cookie shoud be transmitted over a secure HTTPS connection. `TRUE` indicates that the cookie will only be set if a secure connection exists. Default is FALSE. |
| `httponly` | If set to TRUE, he cookie will be accessible only through the HTTP protocol (the cookie will not be accessible to scripting languages). Using httponly helps reduce identity theft using XSS attacks. Default is FALSE. |


Creating a cookie named "user" with the value "John"
```php
<?php
  $value = "John";
  setcookie("user",$value.time() + (86400 * 30),'/');

  if(isset($_COOKIE['user'])){
    echo "Value is: ".$_COOKIE['user'];
  }
  // Outputs "Value is: John" 
?>
```

# MODULE 8: WORKING WITH FILES

## WRITING TO A FILE

- PHP offers a number of functions to use when creating, reading, uploading, and editing files

- The `fopen()` function creates or opens a file
  - If you use `fopen()` with a file that does not exist, the file will be created given that the file has been opened for writing(w) or appending(a)


| Mode | Description |
| --- | --- |
| `r` | Opens file for read only |
| `w` | Opens file for write only. Erases the contents of the file or creates a new file if it does not exist | 
| `a` | Opens file for write only - append |
| `x` | Creates a new file for write only |
| `r+` | Opens file for read/write |
| `w+` | Opens file for read/write. Erases the content of the file or creates a new file if the file does not exist |
| `a+` | Opens file for read/write. Creates a new file if the file does not exist  |
| `x+` | Creates a new file for read/write |

Creating a new file called "file.txt" that will be created in the same directory that houses the PHP code:
```php
<?php
  $myfile = fopen("file.txt","w");
?>
```

- When writing to a file, use the `fwrite()` function

- The first parameter of `fwrite()` is the file to write to, the second is the string to be written

- Use `fclose()` to close the file 

```php
<?php
$myfile = fopen("names.txt", "w");

$txt = "John\n";
fwrite($myfile, $txt);
$txt = "David\n";
fwrite($myfile, $txt);

fclose($myfile);

/* File contains:
John
David
*/
?>
```

## APPENDING TO A FILE

```php
<?php
$myFile = "test.txt";
$fh = fopen($myFile, 'a');
fwrite($fh, "Some text");
fclose($fh);
?>
```

Example form that adds filled-in data to a file:
```html
<?php
if(isset($_POST['text'])) {
  $name = $_POST['text'];
  $handle = fopen('names.txt', 'a');
  fwrite($handle, $name."\n");
  fclose($handle); 
}
?>
<form method="post">
  Name: <input type="text" name="text" /> 
  <input type="submit" name="submit" />
</form>
```

- Each time a name is filled in, it's added to the "names.txt" file, along with a new line

- The `isset()` function determined whether the form had been submitted as well as whether the text contained a value

*NOTE: We did not specify an action attribute for the form, so it will submit to itself*


## READING A FILE

- The `file()` fucntion reads the entire file into an array
  - Each element within the array corresponds to a line in the file

This prints all of the lines in the file and separates them in an array
```php
<?php
  
  $read = file('names.txt');
  foreach($read as $line){
    echo $line . ",";
  }
?>
```

*We used the foreach loop, because the $read variable is an array*

- At the end of the output of the previous example, we would have a comma, as we print it after each element of the array

The following code lets us avoid printing that final comma:
```php
<?php

$read = file('names.txt');
$count = count($read);
$i = 1;
foreach ($read as $line) {
  echo $line;
  if($i < $count) {
    echo ', ';
  }
  $i++;
}

?>
```
- The `$count` variable uses the count function to obtain the number of elements in the `$read` array
  - Then, in the foreach loop, after each line prints, we determine whether the current line is less than the total number of lines and print a comma if it is


# MODULE 9: OBJECT-ORIENTED PHP

- *Object Oriented Programming* (OOP) is a programming style that is intended to make thinking about programming closer to thinking about the real world.

- Objects are created using classes, which are the focal point of OOP.
The class describes what the object will be, but is separate from the object itself. In other words, a class can be thought of as an object's blueprint, description, or definition.

## OBJECTS AND CLASSES

- In PHP, a class can include member variables called properties for defining the features of an object, and functions, called methods, for defining the behavior of an object

- A class definition begins with the keyword class, followed by a class name

- Curly braces enclose the definitions of the properties and methods belonging to the class

```php
class Person {
  public $age; //property
  public function speak() { //method
    echo "Hi!"
  }
}
```

- The code above defines a Person class that includes an age property and a `speak()` method

- Notice the keyword public in front of the speak method; it is a visibility specifier

- The public keyword specifies that the member can be accessed from anywhere in the code

- The process of creating an object of a class is called *instantiation*

To instantiate an object of a class, use the keyword `new`:
```php
$bob = new Person();
```

- In the code above, `$bob` is an object of the `Person` class

To access the properties and methods of an object, use the arrow `->` construct:
```php
echo $bob->age;
```

- This statement outputs the value of the age property for `$bob`

- If you want to assign a value to a property use the assignment operator as you would with any variable

Defining the Person class, instantiate an object, make an assignment, and call the `speak()` method:
```php
class Person {
  public $age;
  function speak() {
    echo "Hi!";
  }
}
$p1 = new Person(); //instantiate an object
$p1->age = 23; // assignment 
echo $p1->age; // 23
$p1->speak(); // Hi!
```

- `$this` is a pseudo-variable that is a reference to the calling object

- When working within a method, use `$this` in the same way you would use an object name outstide of the class

Example:
```php
class Dog {
  public $legs=4;
  public function display() {
    echo $this->legs;
  }
}
$d1 = new Dog();
$d1->display(); //4

$d2 = new Dog();
$d2->legs = 2;
$d2->display(); //2
```

- We created two objects of the Dog class and called their `display()` methods

- Because the `display()` uses `$this`, the legs value referred to the appropriate calling object's property value

## CONSTRUCTOR AND DESCTRUCTOR

- PHP provides constructor magic method `__construct()`, which is called automatically whenever a new object is instantiated

```php
class Person {
  public function __construct() {
    echo "Object created";
  }
}
$p = new Person();
```

- The `__construct()` method is often used for any initialization that the object may need before it is used

```php
class Person {
  public $name;
  public $age;
  public function __construct($name, $age) {
    $this->name = $name;
    $this->age = $age;
  }
}
$p = new Person("David", 42);
```

- In the code above, the constructor uses arguments in the new statement to initialize corresponding class properties

- Similar to the class constructor, there is a destructor magic method `__destruct()`, which is automatically called when an object is destroyed

```php
class Person {
  public function __destruct() {
    echo "Object destroyed";
  }
}
$p = new Person();
```

- This script creates a new person object

- When the script ends the object is automatically destroyed, which calls the destructor and outputs the message "Object destroyed"

- To explicitly trigger the destructor, you can destroy the object using the unset() function in a statement similar to `unset($p);`

## CLASS INHERITANCE

- Classes can inherit the methods and properties of another class

- The class that inherits the methods and properties is called a *subclass*

- The class a subclass inherits from is called the *parent class*

Inheritance is achieved using the `extends` keyword:
```php
class Animal {
  public $name;
  public function hi() {
    echo "Hi from Animal";
  }
}
class Dog extends Animal {
}

$d = new Dog();
$d->hi();
```

- Here the `Dog` class inherits from the `Animal` class. As you can see, all the properties and methods of `Animal` are accessible to `Dog` objects

- Parent constructors are not called implicitly if the subclass defines a constructor

- However, if the child does not define a constructor then it will be inherited from the parent class if it is not declared *private*

*Notice all our properties and methods have public visibility. For added control over objects, declare methods and properties using a visibility keyword.This controls how and from where properties and methods can be accessed.*

- *Visibility* controls how and from where properties and methods can be accessed

- So far we have only use `public`, there are two more keywords to declare visiblity:
  - `protected`: makes members accessible only within the class itself, by inheriting, and by parent classes
  - `private`: makes members accessible only by the class

- Class properties must always have a visibility type

- Methods declared without any explicit visibility keyword are defined as public

*Protected members are used with inheritance*

*Private members are used only internally in a class*



- An *interface* specifies a list of methods that a class must implement
  - However, the interface itself does not contain any method implementations
  - This is an important aspect of interfaces because it allows a method to be handled differently in each class that uses the interface

- The `interface` keyword defines an interface

- The `implements` keyword is used in a class to implement an interface

For example, `AnimalInterface` is defined with a declaration for the `makeSound()` function, but it isn't implemented until it is used in a class:
```php
<?php
interface AnimalInterface {
  public function makeSound();
}

class Dog implements AnimalInterface {
  public function makeSound() {
    echo "Woof! <br />";
  }
}
class Cat implements AnimalInterface {
  public function makeSound() {
    echo "Meow! <br />";
  }
}

$myObj1 = new Dog();
$myObj1->makeSound();

$myObj2 = new Cat();
$myObj2->makeSound();
?>
```

A class can implement multiple interfaces. More than one interfaces can be specified by separating them with commas. For example:
```php
class Demo implements AInterface, BInterface, CInterface {
  // Functions declared in interfaces must be defined here
}
```
- An interface can be inherit another interface by using the `extends` keyword

*All the methods specified in an interface require public visibility*

- *Abstact Classes* can be inherited but they cannot be instantiated

- They offer the advantage of being able to contain both methods with definitions and abstract methods that aren't defined until they are inherited

- A class inheriting from an abstract class must implement all the abstract methods

- The `abstract` keyword is used to create an abstract class or an abstract method

Example:
```php
<?php
abstract class Fruit { 
  private $color; 
    
  abstract public function eat(); 
    
  public function setColor($c) { 
    $this->color = $c; 
  } 
} 

class Apple extends Fruit {
  public function eat() {
    echo "Omnomnom";
  }
}

$obj = new Apple();
$obj->eat();
?>
```

```php
abstract class Calc { 
  abstract public function calculate($param); 
  protected function getConst() { return 4; }
} 
class FixedCalc extends Calc {
  public function calculate($param) {
    return $this->getConst() + $param;
  }
}
$obj = new FixedCalc();
echo $obj->calculate(38);

// Outputs 42
```

- The PHP `static` keyword defines static properties and static methods

- A static property/method of a class can be accessed without creating an object of that class

- A static property or method is accessed by using the *scope resolution operator* `::` between the class name and the property/method name

Example:
```php
<?php
class myClass {
  static $myStaticProperty = 42;
}

echo myClass::$myStaticProperty;
?>
```

- The `self` keyword is needed to access a static property form a static method in a class definition

Example:
```php
<?php
class myClass {
  static $myProperty = 42;
  static function myMethod() {
    echo self::$myProperty;
  }
}

myClass::myMethod();
?>
```

- The PHP `final` keyword defines methods that cannot be overridden in child classes

-  Classes that are defined final cannot be inherited

This example demonstrates that a final method cannot be overridden in a child class:
```php
<?php
class myClass {
  final function myFunction() {
    echo "Parent";
  }
}
// ERROR because a final method cannot be overridden in child classes.
class myClass2 extends myClass {
  function myFunction() {
    echo "Child";
  }
}
?>
```

```php
<?php
final class myFinalClass {
}

// ERROR because a final class cannot be inherited.
class myClass extends myFinalClass {
}
?>
```

*Unlike classes and methods, properties cannot be marked `final`*

Guitarist class that implements the MusicianInterface and calls the `play()` method o nthe Guitarist object:
```php
 
interface MusicianInterface {
  public function play();
}
class Guitarist implements MusicianInterface {
  public function play() {
    echo "La La La";
  }
}
$obj = new Guitarist();
$obj-> play();
```